<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class UserviewController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$viewOwnUser = $auth->getPermission('viewOwnUser');
		$auth->remove($viewOwnUser);
		
		$rule = new \app\rbac\OwnUserRule;
		//$auth->add($rule);
				
		$viewOwnUser->ruleName = $rule->name;		
		$auth->add($viewOwnUser);	
	}
}