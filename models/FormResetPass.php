<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
 
class FormResetPass extends model{
 
    public $email;
    public $password;
	public $password_repeat;
	public $verification_code;
	public $recover;
     
    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat', 'verification_code', 'recover'], 'required', 'message' => 'שדה חובה'],
           
            ['email', 'email', 'message' => 'מייל לא תקין'],
            ['password',  'match', 'pattern' => "/^.{3,16}$/", 'message' => 'הסיסמה צריכה להכיל מינימום 3 תוים.'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'הסיסמאות לא תואמות'],
        ];
    }
	
	public function attributeLabels()
    {
        return [
           
           
            'email' => 'מייל',
			'password' => 'סיסמה',
			'password_repeat' => 'הכנס סיסמה בשנית',
			'verification_code' => 'קוד אימות',  
            
        ];
    }	
}