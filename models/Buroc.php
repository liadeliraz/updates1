<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;	
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "buroc".
 *
 * @property string $subject
 * @property string $treatment
 * @property integer $bstatus
 * @property string $DueDate
 * @property string $creatDate
 * @property string $notes
 *
 * @property Bstatus $bstatus0
 */
class Buroc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buroc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'treatment', 'bstatus', 'DueDate'], 'required','message'=>'שדה חובה'],
            [['bstatus'], 'integer'],
            [['DueDate', 'creatDate'], 'safe'],
            [['subject'], 'string', 'max' => 100],
            [['treatment', 'notes'], 'string', 'max' => 255],
            [['bstatus'], 'exist', 'skipOnError' => true, 'targetClass' => Bstatus::className(), 'targetAttribute' => ['bstatus' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject' => 'נושא',
            'treatment' => 'אופן טיפול',
            'bstatus' => 'סטטוס',
            'DueDate' => 'תאריך יעד',
            'creatDate' => 'תאריך יצירה',
            'notes' => 'הערות',
        ];
    }

	public function getBstatusItem()
    {
        return $this->hasOne(Bstatus::className(), ['id' => 'bstatus']);
    }
	
	
	
    /**
     * @return \yii\db\ActiveQuery
     */
    
}
