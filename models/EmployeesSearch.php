<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employees;

/**
 * EmployeesSearch represents the model behind the search form about `app\models\Employees`.
 */
class EmployeesSearch extends Employees
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role', 'Percent_of_jobs', 'cellphone'], 'integer'],
             [['first_name', 'last_name', 'adress', 'armed','status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		
		$this->role == -1 ? $this->role = null : $this->role; 
		$this->armed == -1 ? $this->armed = null : $this->armed; 
		 $this->Percent_of_jobs == -1 ? $this->Percent_of_jobs = null : $this->Percent_of_jobs; 
		//$this->status == -1 ? $this->status = null : $this->status; 
		//$this->status = 0;
		$this->status == null ? $this->status = 0 :
		$this->status ==1 ? $this->status: $this->status = 0;
		//$this->status ==0 ? $this->status;
		
		
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'Percent_of_jobs' => $this->Percent_of_jobs,
            'cellphone' => $this->cellphone,
			'status' => $this->status,
        ]);
		
        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'adress', $this->adress])
            ->andFilterWhere(['like', 'armed', $this->armed])
			 ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
