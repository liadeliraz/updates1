<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "revenues".
 *
 * @property string $date
 * @property string $day
 * @property integer $cash_desk_784
 * @property integer $cash_desk_782
 * @property integer $store
 */
class Revenues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'revenues';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'cash_desk_784','cash_desk_782',  'store'], 'required','message'=>'שדה חובה'],
            [['date'], 'safe'],
            [['cash_desk_784', 'cash_desk_782', 'store'], 'integer','message'=>'ספרות בלבד'],
           // [['day'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'תאריך',
            //'day' => 'יום',
            'cash_desk_784' => 'קופה 784 [₪]',
            'cash_desk_782' => 'קופה 782 [₪]',
            'store' => 'חנות [₪]',
			
        ];
    }
}
