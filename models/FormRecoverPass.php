<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
 
class FormRecoverPass extends model{
 
    public $email;
     
    public function rules()
    {
        return [
            ['email', 'required', 'message' => 'שדה חובה'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'מינימום 5, מקסימום 80 תוים'],
            ['email', 'email', 'message' => 'מייל לא תקין'],
        ];
    }
	
	
	public function attributeLabels()
    {
        return [
           
           
            'email' => 'הכנס מייל לצורך שליחת קוד אימות:',
			
            
        ];
    }	
}