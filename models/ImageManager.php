<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image_manager".
 *
 * @property integer $id
 * @property string $name
 * @property integer $item_id
 */
class ImageManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
	
     */
	 public $attachment;
    public static function tableName()
    {
        return 'image_manager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'item_id'], 'required'],
            [['item_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
			[['attachment'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'item_id' => 'Item ID',
        ];
    }
}
