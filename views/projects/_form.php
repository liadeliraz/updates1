<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
//use dosamigos\datepicker\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use kartik\daterange\DateRangePicker;
use kartik\widgets\ActiveForm;

//use kartik\widgets\DatePicker;
//use dosamigos\datepicker\DatePicker;
//use dosamigos\datepicker\DateRangePicker;
use dosamigos\multiselect\MultiSelect;

use app\models\Employees;
/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

    
	
	<?= $form->field($model, 'define_project')->textInput(['maxlength' => true]); ?>

	<?= $form->field($model, 'description_project')->textarea(['maxlength' => true]); ?>

     <?=  $form->field($model, 'team_leader')->widget(Select2::classname(), [
   
    'data' => ['ראש צוות לפרויקט'=> Employees::getEmployees()],
	'language' => 'he',
	'options' => ['required'=> true,
	'dir' => 'rtl',
	
	
	 ],
    'pluginOptions' => [
	'dir' => 'rtl',
	'allowClear' => true,
	'placeholder' => ' בחר ראש צוות לפרויקט',
    'tags' => true,
	
    ],
]);
?>
	
	
	<?=  $form->field($model, 'employee')->widget(Select2::classname(), [
   
    'data' => ['בחר עובדים'=> Employees::getEmployees()],
	'language' => 'he',
	'addon'=>['prepend'=>['content'=>'<i class="glyphicon glyphicon-user"></i>']],
	'options' => ['required'=> true,
	'dir' => 'rtl',
	
	'multiple' => true,
	 ],
    'pluginOptions' => [
	'dir' => 'rtl',
	'allowClear' => true,
	'placeholder' => ' בחר עובדים',
    'tags' => true,
	
    ],
]);
?>

<?php	if($model->isNewRecord){
	echo $form->field($model, 'progress')->widget(MultipleInput::className(), [
        'max'               => 15,
        'min'               => 0, // should be at least 2 rows
        'allowEmptyList'    => false,
        'enableGuessTitle'  => true,
        'addButtonPosition' => MultipleInput::POS_HEADER // show add button in the header
    ])
    ->label(false);
	}
	/*if(!$model->isNewRecord)
	echo $form->field($model, 'progress')->textInput(['maxlength' => true,'readonly'=>true]);*/

	
	
if(!$model->isNewRecord){
echo 'מעקב התקדמות<hr>';
if($model->progress!==''){
$model->progress=explode(', ',$model->progress);//converting to string...
$list = [];
for($nq=0;$nq<sizeof($model->progress);$nq++){
$list[$model->progress[$nq].' - בוצע'] = $model->progress[$nq];
}
$checkedList = [1, 2]; //get selected value from db if value exist
     $model->doneprogress = explode(', ',$model->doneprogress);
echo $form->field($model, 'doneprogress')->checkboxList($list)->label(FALSE); 
for($p = 0; $p<sizeof($model->progress);$p++){
 //Html::checkbox($model->progress[$p] . "- בוצע", false, ['label' => $model->progress[$p],'class'=>'btn btn-success']);


//echo '<br>';
}
}
}
echo '<hr>';
?>

  
<?= 
	 $form->field($model, 'due_date', [
    'addon'=>['prepend'=>['content'=>'<i class="glyphicon glyphicon-calendar"></i>']],
	
    'options'=>['class'=>'drp-container form-group']
])->widget(DateRangePicker::classname(), [
    'useWithAddon'=>true,
	'convertFormat'=>true,
	//'autoUpdateInput' =>true,
	'language' => 'he',
	'pluginOptions'=>[
        'locale'=>['format' => 'd/m/Y', 'cancelLabel' => 'בטל', 'applyLabel' => 'בחר'],
		
		
		//'dir' => 'rtl',
		'opens'=>'left',
    ],
	
]);  ?>
	

	
	
	
	

	<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>
	
	

	

    <div class="form-group">
	בעת לחיצה על כפתור "שלח" ישלח מייל לעובדים הנ"ל עם פרטי הפרויקט.
	<hr>
        <?= Html::submitButton($model->isNewRecord ? 'שלח' : 'עדכן', ['id'=>'sendme','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php $this->registerJsFile('app/web/main33.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 

<?php
	
        Modal::begin([
                'header'=>'<h4>מסך שליחה</h4>',
				'id' => 'ssemage',
                'size'=>'modal-sm',
				// 'toggleButton' => ['label' => 'click me'],
            ]);
     
        echo "<div id='sendmeimage'></div>";
		?>
		<hr>
		<button type="button" class="btn btn-default active" data-dismiss="modal">סגור</button>
     <?php
        Modal::end();
    ?>
	
    <?php ActiveForm::end(); ?>

</div>