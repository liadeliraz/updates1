<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\jui\ProgressBar;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'פרויקטים';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
	<?php
	 if (\Yii::$app->user->can('createUser')){
	 ?>
        <?= Html::a('צור פרויקט', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export1'], ['class' => 'btn btn-info']) ?>
			<?php } ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'define_project',
           // 'team_leader',
			[ // the team_leader name of the project
				'attribute' => 'team_leader',

				'label' => 'ראש צוות',
				'format' => 'raw',
			//	'value' => Html::a($model->employeessProject->fullname, 
				'value' =>function($model){ return $model->employeesssProject->fullname;},
			
			],
          //  'employee',
			/*[
				'attribute' => 'employee',
				'label' => 'עובדים',
				'format' => 'raw',
				 'value' => function($model){ return $model->employeesnames;},
			
			],	*/	

				[
				'attribute' => 'employee',
				'label' => 'עובדים',
				'format' => 'raw',
				'value' => function($model){
					return $model->employeesnames;
				},
				//'filter'=>Html::dropDownList('EmployeesSearch[armed]', $armed, $armeds, ['class'=>'form-control']),
			],						
            'location',
			'start',
			[
				'attribute' => 'due_date',
				'value' => 'due_date',
				'format' => 'raw',
				/*'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'due_date',
						'clientOptions' => [
						'autoclose' => true,
						'format' => 'yyyy-m-dd']
							
						])*/
			],
			[
    'class' => \yiister\grid\widgets\ProgressColumn::className(),
    'attribute' => 'doneprogress',
	'label' => 'התקדמות',
    'size' => \yiister\grid\widgets\ProgressColumn::SIZE_LARGE,
    'isStriped' => true,
    'progressBarClass' => function ($model, $column) {
	
        return $model->{$column->attribute} > 15
            ? \yiister\grid\widgets\ProgressColumn::STYLE_SUCCESS
            : \yiister\grid\widgets\ProgressColumn::STYLE_WARNING;
    },
],
             

            ['class' => 'yii\grid\ActionColumn'],
        ],
		
    ]); 
	?>

	
</div>