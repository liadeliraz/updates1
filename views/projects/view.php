<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Projects;
use app\models\ProjectsSearch;
use yii\jui\ProgressBar;
use yii\bootstrap\Progress;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = $model->define_project;
$this->params['breadcrumbs'][] = ['label' => 'פרויקטים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?php
	 if (\Yii::$app->user->can('createUser')){
	 ?>
        <?= Html::a('עדכון', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		<?php } 
		$progress = [];
		$doneprogress =[];
		if($model->progress!=='')
		$progress = explode(', ',$model->progress);
		if($model->doneprogress!=='')
		$doneprogress = explode(', ',$model->doneprogress);
		
		$percents = (count($doneprogress)/count($progress))*100;
		echo Progress::widget([
    'percent' => $percents,
	'label' => 'קצב התקדמות - '."$percents%",
    'barOptions' => ['class' => 'progress-bar-success'],
    'options' => ['class' => 'active progress-striped']
]);?>
		
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'define_project',
            //'team_leader',
		[ // the team_leader name of the project
				'label' => $model->attributeLabels()['team_leader'],
				'format' => 'html',
			//	'value' => Html::a($model->employeessProject->fullname, 
				'value' =>$model->employeesssProject->fullname, 
			],
          //'employee',
		  [
		  'attribute' => 'employee',
		  'label'=>'עובדים',
		  'format' => 'raw',
		  'value' => $model->employeesnames,
		  
		  ],
			/*[ // the employees name of the project
				'label' => $model->attributeLabels()['employee'],
				'format' => 'html',
			//	'value' => Html::a($model->employeessProject->fullname, 
				'value' =>$model->employeesssProject->fullname, 
			],*/
            'location',
			'start',
            'due_date',
			'progress',
			'doneprogress',
            'notes',
        ],
    ]) ?>

</div>
