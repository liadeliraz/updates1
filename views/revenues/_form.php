<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Revenues */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="revenues-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'date')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => true, 
		
		
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
		'language' => 'he',
		
		
        'clientOptions' => [
            'autoclose' => true,
			 'clr' => true,
			// 'startView' => 1,
        //'minView' => 0,
       // 'maxView' => 2,
			// 'viewMode' => 'months',
			 //'todayBtn' => true,
			 
			 //'todayHighlight'=>true,
			
		
        'format' => 'mm/yyyy',
        ]
]);?>
	
	



    

    <?= $form->field($model, 'cash_desk_784')->textInput() ?>

    <?= $form->field($model, 'cash_desk_782')->textInput() ?>

    <?= $form->field($model, 'store')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'עדכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
