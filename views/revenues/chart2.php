<?php

use miloschuman\highcharts\Highcharts;
$this->title = 'הכנסות';
$this->params['breadcrumbs'][] = ['label' => 'הכנסות', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>




<div style="display: none">
	<?php
		echo Highcharts::widget([
		'scripts' => [
			'highcharts-more',
			//'themes/grid',
			'highcharts-3d',
			'modules/drilldown'
		]
			
	]);
	?>	
</div>


<div id="chart2"></div>


<?php


$sql = "SELECT  RIGHT(`date`, 4)  as years , SUM( `cash_desk_784`)+ SUM( `cash_desk_782`)+ SUM( `store`) as total
FROM revenues
GROUP BY years";
$rawData = yii::$app->db->createCommand($sql)->queryAll();
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	'name'=>$data['years'],
	'y' => $data['total'] *1,
	'drilldown' => $data ['total']
	];	
}

$max = $rawData[0]['years'];
for($k=0;$k<sizeof($rawData);$k++){
	
	if($rawData[$k]['years']>$max)
		$max = $rawData[$k]['years'];
}
	

$min = $rawData[0]['years'];
for($k=0;$k<sizeof($rawData);$k++){
	
	if($rawData[$k]['years']<$min)
		$min = $rawData[$k]['years'];
}



$main = json_encode($main_data);

$sql = "select  RIGHT(g.`date`, 4)  as ghh
,(SUM(g.`cash_desk_784`)+ SUM(g.`cash_desk_782`)+ SUM(g.`store`)) as total
,(SUM(g1.`cash_desk_784`)+ SUM(g1.`cash_desk_782`)+ SUM(g1.`store`)) as ya
,( SUM(g2.`cash_desk_784`)+ SUM(g2.`cash_desk_782`)+ SUM(g2.`store`))  as fv
,(SUM(g3.`cash_desk_784`)+ SUM(g3.`cash_desk_782`)+ SUM(g3.`store`)) as mr
,(SUM(g4.`cash_desk_784`)+ SUM(g4.`cash_desk_782`)+ SUM(g4.`store`)) as ap
,(SUM(g5.`cash_desk_784`)+ SUM(g5.`cash_desk_782`)+ SUM(g5.`store`)) as ma
,(SUM(g6.`cash_desk_784`)+ SUM(g6.`cash_desk_782`)+ SUM(g6.`store`)) as yo
,(SUM(g7.`cash_desk_784`)+ SUM(g7.`cash_desk_782`)+ SUM(g7.`store`)) as yol
,(SUM(g8.`cash_desk_784`)+ SUM(g8.`cash_desk_782`)+ SUM(g8.`store`)) as ag
,(SUM(g9.`cash_desk_784`)+ SUM(g9.`cash_desk_782`)+ SUM(g9.`store`)) as sp
,(SUM(g10.`cash_desk_784`)+ SUM(g10.`cash_desk_782`)+ SUM(g10.`store`)) oc
,(SUM(g11.`cash_desk_784`)+ SUM(g11.`cash_desk_782`)+ SUM(g11.`store`)) as nv
,(SUM(g12.`cash_desk_784`)+ SUM(g12.`cash_desk_782`)+ SUM(g12.`store`)) as de

from revenues g
left join revenues g1 on g.date = g1.date and LEFT(g.`date`, 2)=01
left join revenues g2 on g.date = g2.date and LEFT(g.`date`, 2) =02
left join revenues g3 on g.date = g3.date and LEFT(g.`date`, 2)=03
left join revenues g4 on g.date = g4.date and LEFT(g.`date`, 2) =04
left join revenues g5 on g.date = g5.date and LEFT(g.`date`, 2) =05
left join revenues g6 on g.date = g6.date and LEFT(g.`date`, 2) =06
left join revenues g7 on g.date = g7.date and LEFT(g.`date`, 2) =07
left join revenues g8 on g.date = g8.date and LEFT(g.`date`, 2) =08
left join revenues g9 on g.date = g9.date and LEFT(g.`date`, 2) =09
left join revenues g10 on g.date = g10.date and LEFT(g.`date`, 2) =10
left join revenues g11 on g.date = g11.date and LEFT(g.`date`, 2) =11
left join revenues g12 on g.date = g12.date and LEFT(g.`date`, 2) =12

group by ghh";

$rawData = yii::$app->db->createCommand($sql)->queryAll();
$sub_data =[];
foreach ($rawData as $data){
	$sub_data[] =[
	'id' => $data['total'],
	'name' => $data['ghh'] *1,
	'data' => [['ינואר',$data['ya']*1],['פברואר',$data['fv']*1],['מרס',$data['mr']*1],['אפריל',$data['ap']*1],['מאי',$data['ma']*1],['יוני',$data['yo']*1],['יולי',$data['yol']*1],['אוגוסט',$data['ag']*1],['ספטמבר',$data['sp']*1],['אוקטובר',$data['oc']*1],['נובמבר',$data['nv']*1],['דצמבר',$data['de']*1]]
	];	
}


$sub = json_encode($sub_data);




?>




<?php	
	
	 $this->registerJs("$(function () {
    // Create the chart
    $('#chart2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text:'הכנסות לאורך השנים: $max - $min'  
			},
      
        xAxis: {
            type: 'category',
			        useHTML: Highcharts.hasBidiBug,
					useHTML:true

			
        },
        yAxis: {
            title: {
                text: ' סך הכנסות  [₪]',
				  useHTML: Highcharts.hasBidiBug,
				  useHTML:true
				  },
				  


        },
		labels: {
				useHTML: Highcharts.hasBidiBug,
				useHTML:true
				},
        legend: {
            enabled: true,
			useHTML:true
        },
		tooltip: {
        useHTML: true
    },
        plotOptions: {
            series: {
				
                borderWidth: 0,
				useHTML:true,

			
				
                dataLabels: {
                    enabled: true,
					useHTML: Highcharts.hasBidiBug,
					            useHTML:true,

                    format: '{point.y}'
                }
            }
			
        },

       

        series: [{
            name: 'שנים',
            colorByPoint: true,
		 useHTML: Highcharts.hasBidiBug,
		 
		
				
            data: $main,
			useHTML:true

        		
        }],
        drilldown: {
            series: $sub,
			useHTML:true,

        }
		
    });
});");
	?>




