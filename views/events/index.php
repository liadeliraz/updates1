 <script>
      function printContent(el)
      {
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
     }
   </script>
   <script>
      function fullScreen(el)
      {
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.download();
         document.body.innerHTML = restorepage;
		 
     }
   </script>
   
  <body> 
  
   
  

<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use kartik\spinner\Spinner;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\mpdf\Pdf;
use yii\imagine\Image;




/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'שיבוץ';
//$this->params['breadcrumbs'][] = $this->title;


 ?>
 
 
 	<div align="center">
					<!--<h1 align="center"  class="glyphicon glyphicon-download" style="cursor:pointer;" title="Click to Download Page as PDF"></h1>-->
				</div>
				<br>
					<br>
				<section id="page_content">
				
				
				
				
				
		
			
 


<!--<h2 style = "color:#ddd12; text-align:center;">שיבוץ</h2>-->
<?php 

$JSEventClick = <<<EOF
function(calEvent, jsEvent, view) {
    //alert('Event: ' + calEvent.title);
    //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
    //alert('ID: ' + $(this).id);
	var date = $(this).attr('data-date');
	id= calEvent.id;
	  // alert('View: ' + id);

	// $(this).remove();
    // change the border color just for fun
			
			
	
	 document.location.href='/app/web/events/view?id=' +calEvent.id;
	 
				
	
		
   
}
EOF;
	\yii2assets\fullscreenmodal\FullscreenModal::begin([
   'header' => '<h4 class="modal-title text-center">שיבוץ מסך מלא</h4>',
   //'header' => '<h4 class="modal-title text-center">שיבוץ מסך מלא</h4>',
  // 'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           //    <button type="button" class="btn btn-primary">Save changes</button>',
		  //'toggleButton' => ['label' => 'מסך מלא','class'=>'btn btn-danger'],
]);
?>

<div>


</div>
 <?php \yii2assets\fullscreenmodal\FullscreenModal::end();?>
 <!--   < ?= Html::a('שלח שיבוץ', ['fullscreen'], ['class' => 'btn btn-info active glyphicon glyphicon-send','margin-right'=>'2%']) ?>
<!-- <button class="btn btn-info active" style="margin-right:2%"; onclick="fullScreen('div1')">מסך מלא</button>-->
 <button  class="btn btn-primary active" onclick="printContent('div1')">הדפס שיבוץ</button>
 <button style="margin-right:0.1%;" id="qxzx" class="btn btn-warning active">הורד שיבוץ</button>
 <?= Html::a('GoogleApi', ['indexapi'], ['class' => 'btn btn-danger']) ?>
 <?php 
 if (\Yii::$app->user->can('createUser')){
 ?>
 <button style="margin-right:0.1%;"  id="deatailsBTN" class="btn btn-info active"> <i style="margin-left:8%;" class = "glyphicon glyphicon-check"></i>נתוני שיבוץ נוספים</button>
 <button style="margin-right:0.1%;"  id="saveme1" class="btn btn-success active"> <i style="margin-left:8%;" class = "glyphicon glyphicon-send"></i>שלח שיבוץ</button>

<?php } ?>
 
 <!--------------------------############################################################################3-----------------> 
<?php    
 
if (isset($_POST['imgBase'])){
$img = $_POST['imgBase'];
echo("<script>console.log('PHP: ".$img."');</script>");

}

?> 
 


<?php

		
        Modal::begin([
                'header'=>'<h4>נתוני שיבוץ</h4>',
				'id' => 'deatails',
                'size'=>'modal-lg',
				// 'toggleButton' => ['label' => 'click me'],
            ]);
     
      
   


 $month = '<div id="monthName"></div>';  
if(!empty($month)) { 
 // echo $month ;

}
$day = '2017-05-01';
$dt = new DateTime("$day");
echo isset($_POST['request2']);
	 $workDays = \app\models\Schedual::Cal_Days_per_month($month,$dt);
	  "בחודש זה ישנם $workDays ימי עבודה רגילים ללא סופי שבוע.";
		
		
		$teken = round($workDays*0.7);
		$houersTeken70 = round($workDays*8.5*0.7);
		$teken100 = round($workDays*1);
		$houersTeken100 = round($workDays*8.5*1);
		
		$year1 = $dt->format('Y');
			$month1 = $dt->format('m');
		
		 $teken40 = 8;
		 $houersTeken40 =8*8.5;
		
		
		
		
	
	 
			echo '<h4 style = "text-decoration: underline; color:#696969"> 100% משרה:</h4>';
			echo '<r style = "color:#2F4F4F">עובדים עם תקן 100% צריכים להיות משובצים '."$teken100".' משמרות, סך הכל השעות שיצטרכו הינם '."$houersTeken100".'</r>';
			echo ' <br>';
			$percent100Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>3])->all();
			$percent100Array = ArrayHelper::getColumn($percent100Sort, 'id');	
			for($k100 = 0; $k100 < sizeof($percent100Array);$k100++)
			{
		
			$get100arr = \app\models\Events::find()->where(['employees'=>$percent100Array[$k100],'month'=>'מאי 2017'])->all();
			$a100Array = ArrayHelper::getColumn($get100arr, 'id');
			
			$countDays100 = sizeof($a100Array);
			if(sizeof($a100Array)>0) {
			$nameOfEmployee100 = \app\models\Employees::findOne($percent100Array[$k100])->fullname;
			$houersEmployee100 = $countDays100*8.5;
			
			$Weekend100Array = ArrayHelper::getColumn($get100arr, 'created_date');
			////// calculate weekends////////////////
			for($FRI100 = 0; $FRI100 < sizeof($Weekend100Array);$FRI100++){
			$myTimew100 = strtotime($Weekend100Array[$FRI100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Fri")
			$houersEmployee100 = $houersEmployee100 - 3.5;
			}
			
			for($SAT100 = 0; $SAT100 < sizeof($Weekend100Array);$SAT100++){
			$myTimew100 = strtotime($Weekend100Array[$SAT100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Sat")
			$houersEmployee100 = $houersEmployee100 - 8.5;
			}
			
			echo " $nameOfEmployee100 שובץ $countDays100 משמרות, סך הכל בשעות $houersEmployee100 ";
			
			if($houersEmployee100 < $houersTeken100){
			$hoserSumHouers100 = round($houersTeken100 - $houersEmployee100);
			$hoserSumDays100 = round($hoserSumHouers100/8.5);
			if($hoserSumDays100!=0){
			echo '<p12 style = "color:red; font-size:bold"> - חסר ל'.$nameOfEmployee100.' '.$hoserSumHouers100.' שעות או- '.$hoserSumDays100.' ימים</p12>';
			}else{
			echo '<p13 style = "color:green; font-size:bold"> - חסר ל'.$nameOfEmployee100.' '.$hoserSumHouers100.' שעות  - שיבוץ תקין</p13>';
			}
			}
			echo '<br>';
			}
			
			}
				echo ' <hr>';
		
			echo '<h4 style = "text-decoration: underline; color:#696969"> 70% משרה:</h4>';
			echo '<r style = "color:#2F4F4F">עובדים עם תקן 70% צריכים להיות משובצים '."$teken".' משמרות, סך הכל השעות שיצטרכו הינם '."$houersTeken70".'.</r>';
			echo ' <br>';
			$percent70Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>2])->all();
			$percent70Array = ArrayHelper::getColumn($percent70Sort, 'id');	
			for($k70 = 0; $k70 < sizeof($percent70Array);$k70++)
			{
			$get70arr = \app\models\Events::find()->where(['employees'=>$percent70Array[$k70],'month'=>'מאי 2017'])->all();
			$a70Array = ArrayHelper::getColumn($get70arr, 'id');
			$countDays = sizeof($a70Array);
			if(sizeof($a70Array)>0) {
			$nameOfEmployee = \app\models\Employees::findOne($percent70Array[$k70])->fullname;
			$houersEmployee = $countDays*8.5;
			
			$Weekend70Array = ArrayHelper::getColumn($get70arr, 'created_date');
			////// calculate weekends////////////////
			for($FRI70 = 0; $FRI70 < sizeof($Weekend70Array);$FRI70++){
			$myTimew70 = strtotime($Weekend70Array[$FRI70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Fri")
			$houersEmployee = $houersEmployee - 3.5;
			}
			
			for($SAT70 = 0; $SAT70 < sizeof($Weekend70Array);$SAT70++){
			$myTimew70 = strtotime($Weekend70Array[$SAT70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Sat")
			$houersEmployee = $houersEmployee - 8.5;
			}
			
			echo " $nameOfEmployee שובץ $countDays משמרות, סך הכל בשעות $houersEmployee ";
			if($houersEmployee < $houersTeken70){
			$hoserSumHouers = round($houersTeken70 - $houersEmployee);
			$hoserSumDays = round($hoserSumHouers/8.5);
			if($hoserSumDays!=0){
			echo '<p12 style = "color:red; font-size:bold"> - חסר ל'.$nameOfEmployee.' '.$hoserSumHouers.' שעות או- '.$hoserSumDays.' ימים</p12>';
			}else{
			echo '<p13 style = "color:green; font-size:bold"> - חסר ל'.$nameOfEmployee.' '.$hoserSumHouers.' שעות  - שיבוץ תקין</p13>';
			}
			}
			echo '<br>';
			}
			
			}
			echo ' <hr>';
			
			echo '<h4 style = "text-decoration: underline; color:#696969"> 40% משרה:</h4>';
			echo '<r style = "color:#2F4F4F">עובדים עם תקן 40% צריכים להיות משובצים '."$teken40".' משמרות, סך הכל השעות שיצטרכו הינם '."$houersTeken40".'.</r>';
			echo ' <br>';
			$percent40Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>0])->all();
			$percent40Array = ArrayHelper::getColumn($percent40Sort, 'id');	
			for($k40 = 0; $k40 < sizeof($percent40Array);$k40++)
			{
			$get40arr = \app\models\Events::find()->where(['employees'=>$percent40Array[$k40],'month'=>'מאי 2017'])->all();
			$a40Array = ArrayHelper::getColumn($get40arr, 'id');
			$countDays40 = sizeof($a40Array);
			if(sizeof($a40Array)>0) {
			$nameOfEmployee40 = \app\models\Employees::findOne($percent40Array[$k40])->fullname;
			$houersEmployee40 = $countDays40*8.5;
			
			$temp40 = 0 ; 
			echo " $nameOfEmployee40 שובץ $countDays40 משמרות, סך הכל בשעות $houersEmployee40 ";
			if($houersEmployee40 > $houersTeken40){
			echo '<p12 style = "color:red; font-size:bold">העובד '.$nameOfEmployee40.' חרג החודש מ- 40% משרה</p12>';
			$temp40 =1; 
			}
			if($temp40 == 0 )
			echo '<p14 style = "color:green; font-size:bold">שיבוץ תקין</p14>';
			echo '<br>';
			}
			
			}
			echo ' <hr>';
			Modal::end();
	 
	 ?>
		<!--	</details>
			
	 <?php
				echo ' <hr>';
	
   
    ?>



<!--------------------------############################################################################3-----------------> 
 <?php    
 
if (isset($_POST['imgBase64'])){
define('UPLOAD_DIR', 'shiboozuploads/');
$img = $_POST['imgBase64'];
$img = str_replace('data:image/png;base64,', '', $img);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);
$file = UPLOAD_DIR . uniqid() . '.png';
$success = file_put_contents($file, $data);
$name = \app\models\Events::Send2($file);
print $success ? $file : 'Unable to save the file.';
}

?> 
				
				
<hr>



	
  <!--  <h1>< ?= Html::encode($this->title) ?></h1>-->

<script>
function myFunction() {
			var div = document.getElementsByClassName("fc-center")[0];
			var monthName = div.getElementsByTagName("h2")[0].innerHTML;
			var x = document.getElementsByClassName("example");
    x[0].innerHTML = monthName;

	}
</script>
	
	
	<?php
	
	//$workDays = \app\models\Schedual::Cal_Days_per_month($currentMonth,$dt);
	
	echo $currentMonth = '<div class="example"></div>'; 
	//function CalDays(){
	//$currentMonth = '<div class="example"></div>'; 
	//echo $currentMonth;
	//echo $newCorrent = $currentMonth;
	$timestamp = strtotime("$currentMonth");
	
	$dt = new DateTime("$timestamp");
	 $year = $dt->format('Y');
	$month = $dt->format('m');
		
	$myTime = strtotime(''.$month.'/01/'.$year);  // Use whatever date format you want
	$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month,$year); // 31
	$workDays = 0;
	

	while($daysInMonth > 0)
		{
		$day = date("D", $myTime); // Sun - Sat
		if($day != "Fri" && $day != "Sat")
        $workDays++;
	
    $daysInMonth--;
    $myTime += 86400; // 86,400 seconds = 24 hrs.
	
	
}
	?>
	
	


<?php
if (\Yii::$app->user->can('createUser'))
 $this->registerJsFile('v2/app/web/main.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 
 <?php $this->registerJsFile('v2/app/web/jspdf.debug.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 


<div class="loader" style="display:none;  position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -50px; //Half of the height of the loading graphic
  margin-left: -50px; //Half of the width of the loading graphic"></div>

<div id="loading-indicator" style="display:none;  position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -50px; //Half of the height of the loading graphic
  margin-left: -50px; //Half of the width of the loading graphic">
<!--img src="http://dkclasses.com/images/loading.gif" id="loading-indicator" style="display:none" />-->


<?= Spinner::widget([
    'preset' => Spinner::LARGE,
    'color' => 'blue',
    'align' => 'center',
	
	
])?>
</div>
    <?php
	
        Modal::begin([
                'header'=>'<h4>שיבוץ</h4>',
				'id' => 'modal',
                'size'=>'modal-lg',
				// 'toggleButton' => ['label' => 'click me'],
            ]);
     
        echo "<div id='modalContent'></div>";
     
        Modal::end();
    ?>
	
	    <?php
	
        Modal::begin([
                'header'=>'<h4>מסך שליחה</h4>',
				'id' => 'mimage',
                'size'=>'modal-sm',
				// 'toggleButton' => ['label' => 'click me'],
            ]);
     
        echo "<div id='savemimage'></div>";
		?>
		<hr>
		<button type="button" class="btn btn-default active" data-dismiss="modal">סגור</button>
     <?php
        Modal::end();
    ?>
	
	
	
	
	<?php
	
	$JSCode = <<<EOF
function(start, end) {
    var title = prompt('Event Title:');
    var eventData;
    if (title) {
        eventData = {
            title: title,
            start: start,
            end: end
        };
        $('#w0').fullCalendar('renderEvent', eventData, true);
    }
    $('#w0').fullCalendar('unselect');
}
EOF;
$JSDropEvent = <<<EOF
function(date) {
    alert("Dropped on " + date.format());
    if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
    }
}
EOF;
/*$JSDropEvent = <<<EOF
function(date) {
    alert("Dropped on " + date.format());
    if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
    }
}
EOF;



$JSCode = <<<EOF
function(start, end) {
   // var title = prompt('Event Title:');
    var eventData;
    if (title) {
        eventData = {
            title: title,
			id: id,
			created_date: created_date,
            start: start,
            end: end
        };
       // $('#w0').fullCalendar('renderEvent', eventData, true);
    }
   // $('#w0').fullCalendar('unselect');
}
EOF;*/

if (\Yii::$app->user->can('createUser')){

?>
<span id="widget" class="widget" field="AGE" roundby="20" description="Patient age, in years">
  <div id="div1" class= "as1">
  <section class = "as" id="canvas">
    
<?= \yii2fullcalendar\yii2fullcalendar::widget(array(
		 'clientOptions' => [
		 'eventClick' => new JsExpression($JSEventClick),
		'drop' => new JsExpression($JSDropEvent),
		  ],
      'events'=> $events,
	        'options' => [
        'lang' => 'he',
		'dayNames' => true,
		
		
		
					],
  ));
?>

</section>
</div>


</span>

<?php } 	

if (!\Yii::$app->user->can('createUser')){

?>
<span id="widget" class="widget" field="AGE" roundby="20" description="Patient age, in years">
  <div id="div1" class= "as1">
  <section class = "as" id="canvas">
    
<?= \yii2fullcalendar\yii2fullcalendar::widget(array(
      'events'=> $events,
	        'options' => [
        'lang' => 'he',
		'dayNames' => true,
		
		
		
					],
  ));
?>

</section>
</div>


</span>

<?php } ?>
		
   </div>

		 </body>