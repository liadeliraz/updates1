<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Events */


//$today = date("  D  d/n/Y     ", strtotime("$model->created_date")); 


/* Set locale to Hebrew */
//setlocale(LC_ALL, 'he');

/* Output: יום חמישי דצמבר 2005 */
 //$today = strftime("%A %e %B %Y", mktime(0, 0, 0, 12, 22, 2005));


//$this->title = '  עובדים ליום';
$this->params['breadcrumbs'][] = ['label' => 'שיבוץ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-create">

   <!-- <h1>< ? = Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
	
    ]) ?>

</div>
