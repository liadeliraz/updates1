<script>
      function printContent(el)
      {
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
     }
   </script>
   <script>
      function saveContent(el)
      {
        /* var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.save();
         document.body.innerHTML = restorepage;*/
		  var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
		 var file_path = 'host/path/file.jpg';
var a = document.createElement('A');
a.href = file_path;
a.download = document.getElementById(el).innerHTML;
document.body.appendChild(a);
a.click();
document.body.removeChild(a);
     }
   </script>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\web\JsExpression;
use yii\helpers\Url;
use metalguardian\Fotorama;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use kartik\spinner\Spinner;
use kartik\color\ColorInput;
/* @var $this yii\web\View */
/* @var $model app\models\Employees */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'עובדים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

 
$this->registerJs("$(function() {
   $('#popupModal').click(function(e) {
     e.preventDefault();
     $('#modal').modal('show').find('.modal-content')
     .load($(this).attr('href'));
   });
});");


    yii\bootstrap\Modal::begin([
	    'header' => '<h2>תמונת עובד</h2>',
		'id' =>'modal',
	
		]);
		
		//echo '<button style="margin-left:1%" class = "btn btn-success active">הורד תמונה</button>';
		//echo '<button class = "btn btn-default active" onclick="printContent(div1)">הדפס תמונה</button>';
		?>
		<!--<button  class="btn btn-success active" onclick="saveContent('div1')">הורד תמונה</button>-->
		<button  class="btn btn-default active" onclick="printContent('div1')">הדפס תמונה</button>
	<?php	echo '<hr>';
		echo '<div align="center">';
		echo '<div  id = "div1" ><img  src = "http://prat.webni.co.il/app/web/Employees_images'.'/' .$model->image . '" ></div>';
		echo '</div>';
		
    yii\bootstrap\Modal::end();
	
	

?>
<div class="employees-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php if (Yii::$app->session->hasFlash('success22')): ?>
  <div class="alert alert-warning alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
 
  <?= Yii::$app->session->getFlash('success22') ?>
  </div>
<?php endif; ?>
    <p>
		

		
        <?= Html::a('עדכון', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
       <!-- < ?= Html::a('מחיקה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>-->
		<?= Html::a('מסמכי עובד', ['files', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </p>
	
	
	
<!--<img style= "weight:100px; height:100px;" src="web/uploads/רם.jpg "></img>-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
			'color',
			// 'status',
           [ // the role name 
		   'label' => $model->attributeLabels()['status'],
				'value' => function($model){
				if($model->status == 0)
				$model->status = "פעיל";
				if($model->status == 1)
				$model->status = "לא פעיל";
				
					return $model->status ;
				},
				
				/*'label' => $model->attributeLabels()['status'],
				'value' => $model->roleItem->name,*/	
			],	
			
			[ // the role name 
				'label' => $model->attributeLabels()['role'],
				'value' => $model->roleItem->name,	
			],			
			
          
			 //'Percent_of_jobs',
			[ // the Percent_of_jobs name 
				'label' => $model->attributeLabels()['Percent_of_jobs'],
				'value' => $model->percent_of_jobsItem->name,	
			],	
			[ // the armed name 
				'label' => $model->attributeLabels()['armed'],
				'value' => $model->armedItem->name,	
			],	
			'email',
            'cellphone',
            'adress',
           			
			/*[
				'attribute' => 'color',
				'value' => 'ColorInput::classname()',
				//'format' => ['color', ['width' => '100', 'height'=> '100']],
				
			],*/
			
				
			[
				'attribute' => 'image',
				
				//'value' => Html:: a('http://localhost/project_04.05.17/masterproject/web/uploads'.'/' .$model->image,['http://localhost/project_04.05.17/masterproject/web/uploads'.'/' .$model->image]),
				'value' => 'http://prat.webni.co.il/app/web/Employees_images'.'/' .$model->image,
				'format' => ['image', ['width' => '100','onClick' => 'function myFunction()','id' => 'popupModal', 'class' => 'btn', 'height'=> '100']]
			
			],
		
        ],
    ]) 
	
	
?>
	

</div>
