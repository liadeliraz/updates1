<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
//use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'עובדים';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור עובד חדש', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-info']) ?>
		
		
		
		
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
			'cellphone',
			'email',
			'color',
        /*[
			'attribute'=>'color',
			'value'=>function ($model) {
				return '<p style = "background-color:'.$model->color .'"></p>';
			},
			'background-color'=> $model->color,

        ],*/

		/*[
        'attribute'=>'color',
        'value'=>function ($model) {
            return ['style' => 'background-color:' 
            . ($model->color == '#ea9999' ? 'red' : 'blue')];
        },
        //'filterType'=>GridView::FILTER_COLOR,
        //'vAlign'=>'middle',
        //'format'=>'raw',
        //'width'=>'150px',
        //'noWrap'=>true
    ],*/
			//'status',
				
           [
				'attribute' => 'status',
				'label' => 'סטטוס',
				'format' => 'raw',
				'value' => function($model){
				if($model->status == 0)
				$model->status = "פעיל";
				if($model->status == 1)
				$model->status = "לא פעיל";
				
					return $model->status ;
				},
				'filter'=>Html::dropDownList('EmployeesSearch[status]', $status, $statuss, ['class'=>'form-control']),
			],		

			
			[
				'attribute' => 'role',
				'label' => 'תפקיד',
				'format' => 'raw',
				'value' => function($model){
					return $model->roleItem->name;
				},
				'filter'=>Html::dropDownList('EmployeesSearch[role]', $role, $roles, ['class'=>'form-control']),
			],			
			
			
				//'Percent_of_jobs',
           [
				'attribute' => 'Percent_of_jobs',
				'label' => 'אחוז משרה',
				'format' => 'raw',
				'value' => function($model){
					return $model->percent_of_jobsItem->name;
				},
				'filter'=>Html::dropDownList('EmployeesSearch[Percent_of_jobs]', $Percent_of_jobs, $PercentOfJobss, ['class'=>'form-control']),
			],		
		   
		   
           // 'armed',
			/*[
				'attribute' => 'armed',
				'label' => 'נשק',
				'format' => 'raw',
				'value' => function($model){
					return $model->armedItem->name;
				},
				'filter'=>Html::dropDownList('EmployeesSearch[armed]', $armed, $armeds, ['class'=>'form-control']),
			],	*/		
 
           // 'adress',
            ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update}'],
        ],
    ]); ?>
</div>