<?php 
use kartik\widgets\FileInput;
 use yii\helpers\Url;
 use yii\helpers\Html;
 use yii\helpers\ArrayHelper;
header('Content-Type: text/html; charset=utf-8');
echo '<h2 align = "center">מסמכים '."$model->fullname".'</h2>';

?>
<?= Html::a('חזור לפרטי עובד', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<hr>
<?php
$initalpreview = array();
$employeeFiles = \app\models\ImageManager::find('name')->where(['item_id'=>$model->id])->all();
$employeeFilesArray = ArrayHelper::getColumn($employeeFiles, 'name');

	  $files=\yii\helpers\FileHelper::findFiles(iconv('UTF-8', 'HEBREW//TRANSLIT', ('Employees_files/'.$model->id)));

   foreach($files as $file) {
	$file1 = str_replace('Employees_files/'.$model->id,'http://localhost/v1/app/web/Employees_files/'.$model->id,$file);
	$file2 = stripslashes($file1);
      //$initalpreview[] = $file1;
	  $initalpreview[] =  'http://kartik-v.github.io/bootstrap-fileinput-samples/samples/pdf-sample.pdf';	  
	 // $initalpreview[] = 'Employees_files/'.$model->id .'/masterproject20.5.17.sql';
	  }

	

	  
	 
echo '<label class="control-label">הוסף מסמכים</label>';
echo FileInput::widget([
    'model' => $model,
    'name' => 'ImageManager[attachment]',
    'options' => ['multiple' => true,'overwriteInitial '=>false],
	 'pluginOptions' => [
		
		'uploadUrl' => \yii\helpers\Url::to(['/employees/upload-files']),
		'browseIcon' => '<i style = "margin-left:5%;" class="glyphicon glyphicon-paperclip"></i> ',
        'browseLabel' =>  'בחר קבצים',
		'removeLabel' =>  'מחק קבצים',
		'uploadLabel' => 'העלה קבצים',
		'allowedFileExtensions'=>['jpg', 'gif', 'png', 'pdf','xls', 'docx', 'txt','DOC'],
		'uploadExtraData' => [
            'ImageManager[item_id]' => $model->id  
        ],
		//'initialPreview'=>$initalpreview,
		/*'initialPreview'=>[
            "http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg",
            "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg"
        ],*/
        'initialPreviewAsData'=>true,
       // 'initialCaption'=>"The Moon and the Earth",
      /*  'initialPreviewConfig' => [
            ['caption' => 'Moon.jpg', 'size' => '873727'],
            ['caption' => 'Earth.jpg', 'size' => '1287883'],
			 ['caption' => 'About.pdf', 'size' => '8000'],
			// {type: "pdf", size: 8000, caption: "About.pdf", url: "/file-upload-batch/2", key: 4},
        ],*/
      //  'overwriteInitial'=>false,
        'maxFileCount' => 30,
		'maxFileSize'=>20000
	],
]);
 echo '<hr>';

	echo '<br><h4>מסמכים קיימים:</h4>';
echo '<hr>';
	   if (isset($employeeFiles[0])) {
        foreach ($employeeFiles as $file) {
            //$nameFicheiro = substr($file->name, strrpos($file->name, '/') + 1);
			//$nameFicheiro2 = stripslashes($nameFicheiro);
			//$nameFicheiro3 = str_replace($model->id,'',$nameFicheiro2);
			
			/*echo  '<a href="#" class="btn btn-info btn-sm">
          <span class="glyphicon glyphicon-eye-open"></span> תצוגה מקדימה
        </a> - ';*/
	
	
            echo Html::a('<span class="glyphicon glyphicon-download-alt text-success "></span>'.$file->name, Url::base().'/'. 'Employees_files/'.$model->id .'/'.$file->name, ['class'=>'btn  btn-default btn-sm '])."<br/>" . "<br/>"  ; // render do ficheiro no browser
        }
		}
?>



