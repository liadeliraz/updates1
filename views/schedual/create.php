<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Schedual */

$this->title = 'הכנס ימים לשיבוץ';
$this->params['breadcrumbs'][] = ['label' => 'שיבוץ לעובד', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedual-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
