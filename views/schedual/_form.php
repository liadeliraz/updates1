﻿<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use app\models\Schedual;
use kartik\widgets\DatePicker;

//use kartik\daterange\DateRangePicker;



/* @var $this yii\web\View */
/* @var $model app\models\Schedual */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedual-form">

    <?php $form = ActiveForm::begin(); ?>

    	<div style=" position: relative; width: 50%;" >
		<?php
		if($model->isNewRecord){
			echo  $form->field($model, 'employeesName')->textInput(['readonly' => true,'value' =>Yii::$app->user->identity->username]); 
			}
		if(!$model->isNewRecord){
			echo  $form->field($model, 'employeesName')->textInput(['readonly' => true]);
			}
			
				if($model->isNewRecord){
			echo $form->field($model, 'employeeId')->hiddenInput(['value' =>Yii::$app->user->identity->id])->label(false);
										}
										?>
			<!--< ? = $form->field($model, 'month')->input('month',['format' => 'php:Y M']) ?>
			< ?= $form->field($model, 'month')->widget(
				DatePicker::className(), [
					
					//'inline' => true, 
					'language' => 'he',
					'template' => '{addon}{input}',
					'clientOptions' => [
					'autoclose' => true,
					'format' => 'MM yyyy',
					//'multidate' => true
        ]
]);?>
			
			< ?= $form->field($model, 'days')->widget(
				DatePicker::className(), [
					// inline too, not bad
					// 'inline' => true, 
					'language' => 'he',
					// modify template for custom rendering
					 'template' => '{addon}{input}',
					//'template' => '<div class="bfh-datepicker" data-format="y-m-d" data-date="today">{input}</div>',
						//<div class="bfh-datepicker" data-format="y-m-d" data-date="today"></div>
        'clientOptions' => [
            'autoclose' => true,
           'format' => 'dd/mm/yyyy',
			'multidate' => true
        ]
]);?>-->

<?php
echo $form->field($model, 'month')->widget(DatePicker::classname(), [
    'language' => 'he',
	 'type' => DatePicker::TYPE_COMPONENT_APPEND,
	'options' => [
	'placeholder' => 'בחר חודש',
	'language' => 'he',
	],
    'pluginOptions' => [
        //'autoclose'=>true,
		  //'orientation' => 'top-left',
		//'multidate' => true,
		'format' => 'MM yyyy',
    ]
]);
?>

<?php
echo $form->field($model, 'days')->widget(DatePicker::classname(), [
    'language' => 'he',
	 'type' => DatePicker::TYPE_COMPONENT_APPEND,
	'options' => [
	'placeholder' => 'בחר ימים לשליחה לשיבוץ',
	'language' => 'he',
	],
    'pluginOptions' => [
        //'autoclose'=>true,
		  // 'orientation' => 'right',
		'multidate' => true,
		'format' => 'dd/mm/yyyy',
		//'orientation' => 'top-right',
    ]
]);
?>


<!--< ?=  $form->field($model, 'id')->widget(Select2::classname(), [
   
    'data' => Schedual::getDays(),
    'language' => 'he',
	
   'options' => ['multiple' => true,'placeholder' => 'בחר עובדים'],
    'pluginOptions' => [
	
        'allowClear' => true,
        'tags' => true,
    ],
]);
?>-->

<!--< ? =  $form->field($model, 'employeeId')->widget(Select2::classname(), [
   
    'data' => Schedual::getDays(),
    'language' => 'he',
	
   'options' => ['multiple' => true,'placeholder' => 'בחר עובדים'],
    'pluginOptions' => [
	
        'allowClear' => true,
        'tags' => true,
    ],
]);
?>
-->
</div>
			
			<!--< ?= $form->field($model, 'days')->widget(
				DatePicker::className(), [
					'language' => 'he',
					'inline' => true,
					'class'=> 'well well-sm',
					'template' => '<div class="bfh-datepicker" data-format="y-m-d" data-date="today">{input}{}</div>',
					'clientOptions' => [
					'autoclose' => true,
					'format' => 'dd/mm/yyyy',
					'multidate' => true
        ]
]);?>

< ?= $form->field($model, 'days')->input('date',['style'=>'background-color: #fff','width'=>'1050px']) ?>
			< ?= $form->field($model,'days')->widget(DatePicker::className(),['clientOptions' => ['defaultDate' => '2014-01-01']]) ?>





<!--< ?php

echo '<div class="input-group drp-container">';
echo DateRangePicker::widget([
    'name'=>'date_range_4',
    'value'=>'01/12/2015',
    'useWithAddon'=>true,
    'pluginOptions'=>[
        'singleDatePicker'=>true,
        'showDropdowns'=>true,
		'multidate' => true
    ]
]) ;
echo '</div>';?>2
< ? php
echo '<div class="input-group drp-container">';
echo DateRangePicker::widget([
    'name'=>'date_range_5',
    'value'=>'2015-10-19 12:00 AM',
    'useWithAddon'=>true,
    'convertFormat'=>true,
    'pluginOptions'=>[
        'timePicker'=>true,
        'timePickerIncrement'=>15,
        'locale'=>['format' => 'Y-m-d h:i A'],
        'singleDatePicker'=>true,
        'showDropdowns'=>true
    ]
]) ;
echo '</div>';
?>







<!--////////////////////////////////////////////////////////////////////////////////////////////////
	<!--< ? = 
  $form->field($model, 'month')            
         ->dropDownList(['ינואר','פברואר','מרץ','אפריל','מאי','יוני','יולי','אוגוסט','ספטמבר','אוקטובר','נובמבר','דצמבר'],['prompt' => 'בחר חודש'])->label("בחר חודש");
 ?>
<!--
<head>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  
  <script>
  $(document).ready(function() {
    $("#datepicker").datepicker();
  });
  </script>
</head>
<body>
<form>
    <input id="datepicker" multiple/>
</form>
</body>

    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>

  <body>
  
    <h3>חישוב נסיעות לעובדים</h3>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: 31.831702 , lng: 35.306956};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHfNO9bw-2csb7EBdtQhe9SGjQ9N-CHgw&callback=initMap">
    </script>
	<!--<iframe src="http://www.govmap.gov.il/" height="200" width="300"></iframe>-->
 

</body>
</html>

	<!--< ?/*= $form->field($model, 'days')->widget(
    DatePicker::className(), [
        // inline too, not bad
		 
		'language' => 'he',
		'class'=> 'well well-sm',
		//'style'=>'background-color: #fff; width:250px',
		         'inline' => true, 

        // modify template for custom rendering
             'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
			 'multidate' => true,
			 'language' => 'he',
			 'multidateSeparator' => true,
			        'format' => 'dd/mm/yyyy',
					'clearBtn' => true,
				
					
        ]
]);?>*/-->





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>