<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Employees;


/* @var $this yii\web\View */
/* @var $model backend\models\Emails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emails-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

   
	
		<?php $form->field($model, 'receiver_name')->textInput(['readonly' => true,'value' =>Yii::$app->user->identity->username]) ?>

	
	<?= $form->field($model, 'receiver_email')->textInput(['maxlength' => 200]) ?>
	
    <?= $form->field($model, 'subject')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'attachment')->fileInput(['class'=>'form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'שלח מייל' : 'עדכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
