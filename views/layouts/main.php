<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
	//if (\Yii::$app->user->can('createUser')){
    NavBar::begin([
		 
        'brandLabel' => 'Prat.webni.co.il',
        //'brandUrl' => 'index',
		//'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
             'options' => ['class' => 'navbar-nav navbar-right'],

        'items' => [
			
			Yii::$app->user->isGuest ?
            ['label' => 'דף הבית', 'url' => ['/site/index']]
			:
			
			Yii::$app->user->isGuest ? : !Yii::$app->user->can('createUser')?
			' '
			:
			[
			'label' => 'שיבוץ', 'linkOptions' => ['class' => 'dropdown-toggle'],
            'items' => [
                
                 '<li class="divider"></li>',
				 
                ['label' => ' יצירה / עריכה', 'url' => ['/events/index']],
				['label' => 'ימים שנשלחו ע"י העובדים', 'url' => ['/schedual/index']],
			
				
				],
			],
			Yii::$app->user->isGuest ? : !Yii::$app->user->can('createUser')?
			' '
			:
			[
            'label' => 'ניהול עובדים', 'linkOptions' => ['class' => 'dropdown-toggle'],
            'items' => [
                
                 '<li class="dropdown-toggle"></li>',
                
                ['label' => 'עובדים', 'url' => ['employees/index']],
				['label' => 'שליחת מיילים', 'url' => ['emails/index']],
				 ['label' => 'פרויקטים', 'url' => ['projects/index']],
				 ['label' => 'משתמשים', 'url' => ['user/index']],
				  
				],
			],
			
			Yii::$app->user->isGuest ? : !Yii::$app->user->can('createUser')?
			' '
			:
			[
			'label' => 'ניהול הזמנות', 'linkOptions' => ['class' => 'dropdown-toggle'],
            'items' => [
                
                 '<li class="divider"></li>',
				 
                ['label' => 'הזמנות', 'url' => ['/invitations/index']],
                ['label' => 'ספקים', 'url' => ['/suppliers/index']],
				['label' => 'פריטים', 'url' => ['/item/index']],
				
				],
			],
			!Yii::$app->user->isGuest ? 
			' '
			
			:
			['label' => 'מי אנחנו', 'url' => ['/site/about']],
			Yii::$app->user->isGuest ? : !Yii::$app->user->can('createUser')?
				 ' '
			
			:
			[
			'label' => 'ניתוח נתונים', 'linkOptions' => ['class' => 'dropdown-toggle'],
            'items' => [
                
                 '<li class="divider"></li>',
                
                ['label' => 'הכנסות', 'url' => ['/revenues/index']],
				['label' => 'מבקרים', 'url' => ['/visitors/index']],
				['label' => 'מנויים', 'url' => ['/subscribers/index']],
				
				
				],
			],
			Yii::$app->user->isGuest ? : !Yii::$app->user->can('createUser')?
			' '
			:
			[
			'label' => 'גרפים', 'linkOptions' => ['class' => 'dropdown-toggle'],
            'items' => [
                
                 '<li class="divider"></li>',
				 
                ['label' => 'הכנסות לפי שנים', 'url' => ['revenues/chart2']],
				['label' => 'מבקרים לפי רבעונים', 'url' => ['visitors/chart1']],
				['label' => 'מנויים לפי שנים', 'url' => ['subscribers/chart3']],
				
				],
			],
			Yii::$app->user->isGuest ? : !Yii::$app->user->can('createUser')?
			' '
			:
			[
			'label' => 'תפריט עובדים', 'linkOptions' => ['class' => 'dropdown-toggle'],
            'items' => [
                
                 '<li class="divider"></li>',
				 ['label' => 'שליחת שיבוץ', 'url' => ['/schedual/index']],
				 ['label' => 'צפייה בשיבוץ', 'url' => ['/events/index']],
				 ['label' => 'פרויקטים', 'url' => ['projects/index']],
                 ['label' => 'סיכום יומי', 'url' => ['/summary-day/index']],
				
				['label' => 'הודעות', 'url' => ['/messages/index']],
				['label' => 'שינוי סיסמה', 'url' => ['/user/change_password']],
				
				],
			],
			Yii::$app->user->isGuest ? : Yii::$app->user->can('createUser')?
			' '
			:
			['label' => 'שליחת שיבוץ', 'url' => ['/schedual/index']],
			Yii::$app->user->isGuest ? : Yii::$app->user->can('createUser')?
			' '
			:
				 ['label' => 'צפייה בשיבוץ', 'url' => ['/events/index']],
				 Yii::$app->user->isGuest ? : Yii::$app->user->can('createUser')?
				 ' '
			:
				 ['label' => 'פרויקטים', 'url' => ['projects/index']],
				 Yii::$app->user->isGuest ? : Yii::$app->user->can('createUser')?
				 ' '
			:
                 ['label' => 'סיכום יומי', 'url' => ['/summary-day/index']],
				Yii::$app->user->isGuest ? : Yii::$app->user->can('createUser')?
				' '
			:
				['label' => 'הודעות', 'url' => ['/messages/index']],
				Yii::$app->user->isGuest ? : Yii::$app->user->can('createUser')?
				' '
			:
				['label' => 'שינוי סיסמה', 'url' => ['/user/change_password']],
				
			Yii::$app->user->isGuest ? : !Yii::$app->user->can('createUser')?
			' '
			:
			  
            ['label' => 'משימות', 'url' => ['/buroc/index']],
				
			
			Yii::$app->user->isGuest ? 
			['label' => 'צור קשר', 'url' => ['/site/contact']]
			
			:
           
			'',
			
			
			
			
			
            Yii::$app->user->isGuest ? (
                ['label' => 'כניסה', 'url' => ['site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form','onClick'=>'return confirm("ברצונך לצאת מהמערכת?");' ])
                . Html::submitButton(
                    'יציאה מהמערכת (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
	]);
    NavBar::end();
	//}
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><strong> Webni.co.il  <?= date('Y') ?> &copy; </strong></p>

        <!--<p class="pull-right"><?= Yii::powered() ?></p>-->
		<p class="pull-right"><strong>Powered by Liad Nizri & Eliraz Shimon</strong></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
