<?php

use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;


$this->title = 'מבקרים';
$this->params['breadcrumbs'][] = ['label' => 'מבקרים', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>


<?php
/////////variable of year////////
/*$year = "SELECT DISTINCT YEAR(date) as e
FROM `visitors`";
$yearData = yii::$app->db->createCommand($year)->queryAll();

echo "$yearData->year[e]"; 
/**/
$Varr = [];
$Vquery = \app\models\Visitors::find()->all();
$aVArray = ArrayHelper::getColumn($Vquery, 'date');
 $aVArray= array_unique($aVArray);
 $new_array = array_values($aVArray);

 for($i1=0;$i1<sizeof($new_array);$i1++){
$expDate= explode('/', $new_array[$i1]);
$Varr[]= $expDate[1];
 }
  $Varr= array_unique($Varr);
 $new_array2 = array_values($Varr);

 
//echo '<hr>' .$new_array2[0] .'<hr>';
   
 $submittedValue = "2017";
        $value0 = "2017";
        $value1 = "2018";
        $value2 = "2019";
        $value3 = "2020";
        if (isset($_POST["FruitList"])) {
            $submittedValue = $_POST["FruitList"];
        }else{


                        $submittedValue = '2017';

        }

        ?>
        <!--/////////dropdownlist of year////////-->
   <form action="" name="fruits" method="post"> 
   <h4> בחר שנה</h4> 
         <select project="FruitList" id="FruitList" name="FruitList" class="btn btn-primary" value='$_POST["FruitList"]'>
		 <?php
		 for($i=0;$i<sizeof($new_array2);$i++){
			 ?>
         <option value = "<?php echo $new_array2[$i]; ?>"<?php echo($value0 == $new_array2[$i])?" SELECTED":""?>><?php echo $new_array2[$i]; ?></option>
         <?php } ?>
        </select>
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" value=' $submittedValue' />
        <!--//////necessary for yii//////////-->
        <input  type="submit" name="submit" id="submit" value="בדוק שנה" class="btn btn-primary" />
        <!--////////////////-->
        </form>



<div style="display: none">
	<?php
		echo Highcharts::widget([
		'scripts' => [
			'highcharts-more',
			//'themes/grid',
			'highcharts-3d',
			'modules/drilldown'
		]
			
	]);
	?>	
</div>


<div id="chart1"></div>



<?php	

$sql = "select (SUM(g1.`store`)+ SUM(g2.`store`)+ SUM(g3.`store`)) as revo
,(SUM(g4.`store`)+ SUM(g5.`store`)+ SUM(g6.`store`)) as revt
,(SUM(g7.`store`)+ SUM(g8.`store`)+ SUM(g9.`store`)) as revth
,(SUM(g10.`store`)+ SUM(g11.`store`)+ SUM(g12.`store`)) as revf
from visitors g
left join visitors g1 on g.date = g1.date and (LEFT(g.`date`, 2) =01) and (RIGHT(g.`date`, 4) =2013)
left join visitors g2 on g.date = g2.date and (LEFT(g.`date`, 2) =02) and (RIGHT(g.`date`, 4) =2013)
left join visitors g3 on g.date = g3.date and (LEFT(g.`date`, 2) =03) and (RIGHT(g.`date`, 4) =2013)
left join visitors g4 on g.date = g4.date and (LEFT(g.`date`, 2) =04) and (RIGHT(g.`date`, 4) =2013)
left join visitors g5 on g.date = g5.date and (LEFT(g.`date`, 2) =05) and (RIGHT(g.`date`, 4) =2013)
left join visitors g6 on g.date = g6.date and (LEFT(g.`date`, 2) =06) and (RIGHT(g.`date`, 4) =2013)
left join visitors g7 on g.date = g7.date and (LEFT(g.`date`, 2) =07) and (RIGHT(g.`date`, 4) =2013)
left join visitors g8 on g.date = g8.date and (LEFT(g.`date`, 2) =08) and (RIGHT(g.`date`, 4) =2013)
left join visitors g9 on g.date = g9.date and (LEFT(g.`date`, 2) =09) and (RIGHT(g.`date`, 4) =2013)
left join visitors g10 on g.date = g10.date and (LEFT(g.`date`, 2) =10) and (RIGHT(g.`date`, 4) =2013)
left join visitors g11 on g.date = g11.date and (LEFT(g.`date`, 2) =11) and (RIGHT(g.`date`, 4) =2013)
left join visitors g12 on g.date = g12.date and (LEFT(g.`date`, 2) =12) and (RIGHT(g.`date`, 4) =2013)";

$rawData = yii::$app->db->createCommand($sql)->queryAll();
if(is_array($rawData))
	echo 'yes';
 $new_array3 = array_values($rawData);
var_dump($new_array3);
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	
	'data' => [[$data['revo']*1],[$data['revt']*1],[$data['revth']*1],[$data['revf']*1]]
	];	
}


$main = json_encode($main_data);
$new_array4 = array_values($main_data);
$main1 = json_encode($new_array4);
echo count($main);
?>

<?php		
	$this->registerJs("$(function () {
    $('#chart1').highcharts({		
		
        title: {
            text: 'מבקרים לפי רבעונים לשנת $submittedValue'
        },
        xAxis: {
            categories: ['רבעון 1', 'רבעון 2', 'רבעון 3', 'רבעון 4' ],
			useHTML: true
        },
        labels: {
            items: [{
                html: 'סך הכל כמות מבקרים',
                style: {
                    left: '130px',
                    top: '18px',
					useHTML: true,
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }]
        },
		tooltip: {
        useHTML: true
    },
	legend: {
            enabled: true,
			useHTML:true
        },
        series: [{
            type: 'column',
            name: 'קופה 784',
            data: [150,200,362,260],
			useHTML: true
        }, {
            type: 'column',
            name: 'קופה 782',
            data: [150,200,362,260],
			useHTML: true
        }, {
            type: 'column',
            name: 'חנות',
            data: $main,
			useHTML: true
        }, {
            type: 'spline',
            name: 'ממוצע',
            data: [276.66, 316.66, 439, 356.66],
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white',
				useHTML: true
            }
        }, {
            type: 'pie',
            name: 'סך המבקרים',
            data: [{
                name: 'חנות',
                y: 1472,
                color: Highcharts.getOptions().colors[0] // Jane's color
            }, {
                name: 'קופה 784',
                y: 1669,
                color: Highcharts.getOptions().colors[1] // John's color
            }, {
                name: 'קופה 782',
                y: 972,
                color: Highcharts.getOptions().colors[2] // Joe's color
            }],
            center: [50, 80],
            size: 100,
            showInLegend: false,
			useHTML: true,
            dataLabels: {
                enabled: false,
				useHTML: true
            }
        }]
    });
});");
?>




