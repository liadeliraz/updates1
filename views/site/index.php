<?php
use yii\imagine\Image;

/* @var $this yii\web\View */

$this->title = 'Prat.Webni';
?>
<div class="site-index">
	<?php if (Yii::$app->session->hasFlash('success')): ?>
  <div class="alert alert-success alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
 
  <?= Yii::$app->session->getFlash('success') ?>
  </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('successPmail')): ?>
  <div class="alert alert-success alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
 
  <?= Yii::$app->session->getFlash('successPmail') ?>
  </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('successNewpass')): ?>
  <div class="alert alert-success alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
 
  <?= Yii::$app->session->getFlash('successNewpass') ?>
  </div>
<?php endif; ?>
    <div class="jumbotron">
        <h1 style = "margin-top:1%; color: #420b3a
;
		
		font-family:Copperplate Gothic Light;
	
	font-style: italic;
	font-variant: small-caps;
	font-weight: bold;
	line-height: 45.1px;
		
		font-size: 200%;
		
		"> Prat.Webni.Co.Il </h1>
<?php 	if(Yii::$app->user->isGuest){?>
        <p class="lead" style = " color: #420b3a
">ברוכים הבאים למערכת ניהול שמורת הטבע עין פרת</p>
		        <p><a class="btn btn-lg btn-success" href="http://prat.webni.co.il/app/web/site/login">כניסה למערכת</a></p>
<?php
}
?>
<?php 	if(!\Yii::$app->user->isGuest){?>
        <p class="lead" style = 
		
		
		"font-family: TimesNewRoman;
	font-size: 40px;
	font-style: italic;
	font-variant: normal;
	font-weight: bold;
	line-height: 44px;
	color: #420b3a;">ברוכים הבאים למערכת ניהול שמורת הטבע עין פרת</p>
		        

				 			

<?php
}
?>
<!--
       <p style = "margin:5%"><a class="btn btn-lg btn-success" href="employees">עובדים</a></p>
		        <p style = "margin:3%"><a class="btn btn-lg btn-success" href="buroc">בירוקרטיה</a></p>
		 
		<img src = "tamar" style="width:304px;height:228px;"></img>
	-->
    </div>
<div>
			<script src="<?= Yii::$app->request->baseUrl?>/js/jssor.slider-21.1.6.min.js" type="text/javascript"></script>
			<script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: true,
              $Idle: 0,
              $AutoPlaySteps: 2,
              $SlideDuration: 3400,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 280,
			  $SlideHeight: 280,
              $Cols: 5,
			  $SlideSpacing: 40,
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 809);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*responsive code end*/
        };
    </script>
    <style></style>
    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 980px; height: 350px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('<?= Yii::$app->request->baseUrl?>/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 980px; height: 280px; overflow: hidden;">
            <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="http://liadni.webni.co.il/%D7%9B%D7%95%D7%A1.jpg" />
            </div>
   <div style="display: none;padding:1px; border:3px solid  #420b3a;
   
   background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;
   
   
   
   
   ">
  



  <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/10.jpg" />
            </div>
           
   <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
   <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/22.jpg" />
            </div>
              <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/33.jpg" />
            </div>
            <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/17.jpg" />
            </div>
              <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/18.jpg" />
            </div>
            <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/20.jpg" />
            </div>
              <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/44.jpg" />
            </div>
			   <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/5.jpg" />
				 </div>
               <div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/55.jpg" />
            </div> 
			<div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">
                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/6.jpg" />
            </div>
						
						<div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">

                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/77.jpg" />
            </div>
						<div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">

                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/88.jpg" />
            </div>
			 			<div style="display: none;padding:1px; border:3px solid  #420b3a; background-color: #f0f8ff;
-moz-box-shadow: 0px 0px 20px #000000;
-webkit-box-shadow: 0px 0px 20px #000000;
box-shadow: 0px 20px 20px #000000;">

                <img data-u="image" src="<?= Yii::$app->request->baseUrl?>/img/nevia.jpg" />
            </div>
			
			
			
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->
		</div>
    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <!--<h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
           --> </div>
        </div>

    </div>
</div>

