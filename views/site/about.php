<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'מי אנחנו';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
	<h2 style="font-family:Copperplate Gothic Light;
	
	font-style: italic;
	font-variant: small-caps;
	font-weight: bold;
	line-height: 45.1px;
		
		font-size: 500%;color: #420b3a;"><b> Webni.co.il</h2>
    <div style = "font-size:20px;"><p>
   אנו צוות פיתוח אתרים ומערכות מידע אשר הוקם בשנת 2016
    </p>
 <p>
  הצוות הוקם על ידי שני סטודנטים להנדסת תעשייה וניהול - מערכות מידע, אלירז שמעון וליעד ניזרי.
    </p>
	 <p>
  החזון שלנו הינו לפתח ולשרת קהל רב של ארגונים ועסקים תוך מתן פתרונות תוכנה לייעול העבודה השוטפת וקידום בית העסק או הארגון.
    </p>
	 <p>
אנו מתמקדים בפיתוח סביבות עבודה נוחות ופשוטות אשר יקלו על משתמשי הקצה באשר הם ולכל אדם תהיה היכולת להפעיל את המערכות שלנו.    </p>
   <!-- <code><?//= __FILE__ ?></code>-->
</div>
</div>