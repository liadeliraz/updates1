<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subscribers */

$this->title = ' עידכון מנויים ליום: ' . $model->date;
$this->params['breadcrumbs'][] = ['label' => 'מנויים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->date, 'url' => ['view', 'id' => $model->date]];
$this->params['breadcrumbs'][] = 'עידכון';
?>
<div class="subscribers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
