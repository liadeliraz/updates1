<?php

use miloschuman\highcharts\Highcharts;

$this->title = 'מנויים';
$this->params['breadcrumbs'][] = ['label' => 'מנויים', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>




<div style="display: none">
	<?php
		echo Highcharts::widget([
		'scripts' => [
			'highcharts-more',
			//'themes/grid',
			'highcharts-3d',
			'modules/drilldown'
		]
			
	]);
	?>	
</div>


<div id="chart3"></div>

<?php


$sql = "select  RIGHT(g.`date`, 4)  as ghh
,(SUM(g.`cash_desk_784`)+ SUM(g.`cash_desk_782`)+ SUM(g.`store`)) as rev1
,(SUM(g1.`cash_desk_784`)+ SUM(g1.`cash_desk_782`)+ SUM(g1.`store`)) as ya
,( SUM(g2.`cash_desk_784`)+ SUM(g2.`cash_desk_782`)+ SUM(g2.`store`))  as fv
,(SUM(g3.`cash_desk_784`)+ SUM(g3.`cash_desk_782`)+ SUM(g3.`store`)) as mr
,(SUM(g4.`cash_desk_784`)+ SUM(g4.`cash_desk_782`)+ SUM(g4.`store`)) as ap
,(SUM(g5.`cash_desk_784`)+ SUM(g5.`cash_desk_782`)+ SUM(g5.`store`)) as ma
,(SUM(g6.`cash_desk_784`)+ SUM(g6.`cash_desk_782`)+ SUM(g6.`store`)) as yo
,(SUM(g7.`cash_desk_784`)+ SUM(g7.`cash_desk_782`)+ SUM(g7.`store`)) as yol
,(SUM(g8.`cash_desk_784`)+ SUM(g8.`cash_desk_782`)+ SUM(g8.`store`)) as ag
,(SUM(g9.`cash_desk_784`)+ SUM(g9.`cash_desk_782`)+ SUM(g9.`store`)) as sp
,(SUM(g10.`cash_desk_784`)+ SUM(g10.`cash_desk_782`)+ SUM(g10.`store`)) oc
,(SUM(g11.`cash_desk_784`)+ SUM(g11.`cash_desk_782`)+ SUM(g11.`store`)) as nv
,(SUM(g12.`cash_desk_784`)+ SUM(g12.`cash_desk_782`)+ SUM(g12.`store`)) as de

from subscribers g
left join subscribers g1 on g.date = g1.date and LEFT(g.`date`, 2)=01

left join subscribers g2 on g.date = g2.date and LEFT(g.`date`, 2) =02
left join subscribers g3 on g.date = g3.date and LEFT(g.`date`, 2)=03
left join subscribers g4 on g.date = g4.date and LEFT(g.`date`, 2) =04
left join subscribers g5 on g.date = g5.date and LEFT(g.`date`, 2) =05
left join subscribers g6 on g.date = g6.date and LEFT(g.`date`, 2) =06
left join subscribers g7 on g.date = g7.date and LEFT(g.`date`, 2) =07
left join subscribers g8 on g.date = g8.date and LEFT(g.`date`, 2) =08
left join subscribers g9 on g.date = g9.date and LEFT(g.`date`, 2) =09
left join subscribers g10 on g.date = g10.date and LEFT(g.`date`, 2) =10
left join subscribers g11 on g.date = g11.date and LEFT(g.`date`, 2) =11
left join subscribers g12 on g.date = g12.date and LEFT(g.`date`, 2) =12

group by ghh";
$rawData = yii::$app->db->createCommand($sql)->queryAll();
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	'name'=>$data['ghh'],
	'data' => [[$data['ya']*1],[$data['fv']*1],[$data['mr']*1],[$data['ap']*1],[$data['ma']*1],[$data['yo']*1],[$data['yol']*1],[$data['ag']*1],[$data['sp']*1],[$data['oc']*1],[$data['nv']*1],[$data['de']*1]]	
	];	
}


$max = $rawData[0]['ghh'];
for($k=0;$k<sizeof($rawData);$k++){
	
	if($rawData[$k]['ghh']>$max)
		$max = $rawData[$k]['ghh'];
}


$min = $rawData[0]['ghh'];
for($k=0;$k<sizeof($rawData);$k++){
	
	if($rawData[$k]['ghh']<$min)
		$min = $rawData[$k]['ghh'];
}



$main = json_encode($main_data);


?>

<?php	
	
	$this->registerJs("$(function () {
    $('#chart3').highcharts({
        title: {
            text: 'מגמת מנויים',
            x: -20 //center
        },
        subtitle: {
            text: ' לאורך השנים: $max - $min',
            x: -20
        },
        xAxis: {
            categories: ['ינואר', 'פברואר', 'מרס', 'אפריל', 'מאי', 'יוני',
                'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר']
				
        },
        yAxis: {
            title: {
                text: 'מנויים'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080',
				useHTML:true
            }]
        },
        tooltip: {
            valueSuffix: 'מנויים',
			useHTML:true
        },
		legend: {
            enabled: true,
			useHTML:true
        },
		
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0,
			useHTML:true
        },
        series: $main
    });
});");
?>




