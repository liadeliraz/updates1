<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Suppliers;
use app\models\Item;
use dosamigos\datepicker\DatePicker;
use kartik\widgets\TouchSpin;
/* @var $this yii\web\View */
/* @var $model app\models\Invitations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invitations-form">

    <?php $form = ActiveForm::begin(); ?>

 
	 <?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'supplier_name')->dropDownList(Suppliers::getSupplierss(),['prompt' => 'בחר ספק']) ?>
   <!-- < ?= $form->field($model, 'file')->fileInput()->label('הצעת מחיר') ?>-->

   
	<?=$form->field($model, 'quantity_order')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'הכנס כמות'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
    
   

    <?= $form->field($model, 'approval_status')->dropDownList([ 'מאושר' => 'מאושר', 'לא מאושר' => 'לא מאושר', ], ['prompt' => 'בחר סטטוס']) ?>

    <?= $form->field($model, 'order_status')->dropDownList([ 'סופק' => 'סופק', 'לא סופק' => 'לא סופק', ], ['prompt' => 'בחר סטטוס']) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'עידכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
