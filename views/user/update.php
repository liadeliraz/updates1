<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'עדכון משתמש:'.$model->username .', '.'שם עובד: '. \app\models\Employees::findOne($model->id)->fullname;
$this->params['breadcrumbs'][] = ['label' => 'משתמשים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'עדכון';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'roles' => $roles,
    ]) ?>

</div>
