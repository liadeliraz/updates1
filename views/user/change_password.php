<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;


$this->title = 'שינוי סיסמה ';

$this->params['breadcrumbs'][] = $this->title;
?>
<div align="center" style= "margin-top:2%;font-size:25px;color:	#000080">
<?php
echo "שינוי סיסמא: ";
?>
<?=  Yii::$app->user->identity->username?>

</div>
<br>


 
<?php $form = ActiveForm::begin(); ?>

<?=$form->field($user, 'currentPassword')->passwordInput()->label('סיסמה נוכחית') ?>

<?=$form->field($user, 'newPassword')->passwordInput()->label('סיסמה חדשה') ?>

<?=$form->field($user, 'newPasswordConfirm')->passwordInput()->label('אשר סיסמה חדשה') ?>

<div class="form-group">
    
	     <?= Html::submitButton('שנה סיסמה',['class' => 'btn btn-primary']) ?>
      
    </div>
</div>

 <?php ActiveForm::end(); ?>