<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Employees;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


	<?php if($model->isNewRecord){ ?>
		<?=$form->field($model, 'id')->dropDownList(User::getNewUsers(),['prompt' => 'בחר עובד']) ?> 
	<?php } ?>
	<?php if(!$model->isNewRecord){ 
	   $form->field($model, 'id')->textInput() ?>
	<?php } ?>
	
		<?= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>		
	


    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>



	
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'עדכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
