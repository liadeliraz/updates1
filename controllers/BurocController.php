<?php

namespace app\controllers;

use Yii;
use app\models\Buroc;
use app\models\BurocSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Bstatus;
use yii\filters\AccessControle;
use yii\web\UnauthorizedHttpException;
/**
 * BurocController implements the CRUD actions for Buroc model.
 */
class BurocController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
       return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete','export'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Buroc models.
     * @return mixed
     */
    public function actionIndex()
    {
	if (!\Yii::$app->user->can('indexBuroc'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
	if(Yii::$app->user->isGuest){
		return $this->goHome();
		}
        $searchModel = new BurocSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'bstatuss' => Bstatus::getBstatussWithAllBstatuss(),
			'bstatus' => $searchModel->bstatus,
        ]);
    }
	
	public function actionExport()
    {
		$burocExport= Buroc::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $burocExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['subject','treatment','bstatus','DueDate','creatDate','notes'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['subject' => 'נושא', 'treatment' => 'אופן טיפול','bstatus' => 'סטטוס', 'DueDate' => 'תאריך יעד', 'creatDate' => 'תאריך יצירה', 'notes' => 'הערות'], 
			'fileName' => 'Bureaucracy',
	
		]);
	}

    /**
     * Displays a single Buroc model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
	
		if (!\Yii::$app->user->can('viewBuroc'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Buroc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	
		if (!\Yii::$app->user->can('createBuroc'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = new Buroc();

        if ($model->load(Yii::$app->request->post())) 
		{
			 $localtime = localtime();
	         $model->creatDate = date('d-m-Y h:m:s');
             $model->save();
            return $this->redirect(['view', 'id' => $model->subject]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Buroc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	
		
        $model = $this->findModel($id);
		if (!\Yii::$app->user->can('updateBuroc'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->subject]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Buroc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('deleteBuroc'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
	
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Buroc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Buroc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Buroc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
