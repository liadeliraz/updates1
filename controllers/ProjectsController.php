<?php

namespace app\controllers;

use Yii;
use app\models\Projects;
use yii\db\Query;
use app\models\ProjectsSearch;
use PHPExcel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use app\models\Employees;
use app\models\Emails;
use yii\filters\AccessControle;
use yii\jui\ProgressBar;
use yii\bootstrap\Progress;
/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
       return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete','export'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (!\Yii::$app->user->can('indexProject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionExport()
    {
		$employeeExport= Projects::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $employeeExport,
			'format' => 'Excel2007',
			'mode' => 'export', //default value as 'export'
			'columns' => ['id','define_project','description_project',['attribute' => 'team_leader','value' => function($model) {
                        return $model->employeesssProject->fullname;
                    }],['attribute' => 'employee','value' => function($model) {
                        return $model->employeesnames;
                    }],'start','due_date','progress','doneprogress','location','notes'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['id' => 'מספר פרויקט', 'define_project' => 'שם הפרויקט','description_project' => 'תיאור הפרויקט','team_leader' => 'ראש צוות', 'employee' => 'עובדים','start' =>'תאריך התחלה', 'due_date' => 'תאריך סיום','progress' =>'שלבי הפרויקט','doneprogress' =>'התקדמות הפרויקט', 'location' => 'מיקום', 'notes' => 'הערות'], 
			'fileName' => 'Projects',
	
		]);
	}

	public function actionExport1()
    {
				 $query = new Query;

				  
		  $query->select('employeesName, days')
    ->from('schedual')
    ->limit(10);
			//$result = mysql_query($query);
			//confirm_query($result);
			$objPHPExcel = new PHPExcel(); 
			$objPHPExcel->setActiveSheetIndex(0); 

			$rowCount = 1; 
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'employeesName');
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'days');
			

			/*while($row = mysql_fetch_array($result)){ 
				$rowCount++;
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $row['0']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $row['1']);
				
			}*/ 

			//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
			//$objWriter->save('some_excel_file.xlsx'); 
			
	}
    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

		if (!\Yii::$app->user->can('viewProject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        
				$model = new Projects();
				//$emails = new Emails();
				
                if(isset($_POST['Projects']))
                {
                        $model->attributes=$_POST['Projects'];
                        if($model->employee!==''){
						$model->progress=implode(', ',$model->progress);//converting to string...
							$EmployeesThatInProject = implode(', ',\app\models\Employees::getEmployeesPerDays($model->employee));
							for($i=0; $i<sizeof($model->employee); $i++){
							////send to employees
								$Mail = \app\models\Employees::getEmployeesEmailsToProjects($model->employee[$i]);
								$TeamLeaderName = \app\models\Employees::getTeamLeaderTprojects($model->team_leader);
								$value = Yii::$app->mailer->compose()
								->setFrom([ 'prat@webni.co.il' => 'prat@webni.co.il' ])
								->setTo($Mail)
								->setSubject('שובצת לפרויקט חדש')
								->setHtmlBody('<div style="text-align:right;"><h4>:שם הפרויקט</h4>'.$model->define_project.'<h4>:תיאור הפרויקט</h4>'.$model->description_project.'<h4>:ראש צוות</h4>'."$TeamLeaderName".'<h4>:עובדים ששובצו לפרויקט</h4>'.$EmployeesThatInProject.'<h4>:תאריך יעד</h4>'.$model->due_date.'<h4>:מיקום</h4>'.$model->location.'<h4>:הערות</h4>'.$model->notes.'</div>')
								->send();    
							
								
							}
							
								////send to team leader
								$Mail = \app\models\Employees::getEmployeesEmailsToProjects($model->team_leader);
								$TeamLeaderName = \app\models\Employees::getTeamLeaderTprojects($model->team_leader);
								$value = Yii::$app->mailer->compose()
								->setFrom([ 'prat@webni.co.il' => 'prat@webni.co.il' ])
								->setTo($Mail)
								->setSubject('שובצת לפרויקט חדש')
								->setHtmlBody('<div style="text-align:right;"><h4>:שם הפרויקט</h4>'.$model->define_project.'<h4>:תיאור הפרויקט</h4>'.$model->description_project.'<h4>:ראש צוות</h4>'."$TeamLeaderName".'<h4>:עובדים ששובצו לפרויקט</h4>'.$EmployeesThatInProject.'<h4>:תאריך יעד</h4>'.$model->due_date.'<h4>:מיקום</h4>'.$model->location.'<h4>:הערות</h4>'.$model->notes.'</div>')
								->send();    
								
                                $model->employee=implode(', ',$model->employee);//converting to string...
								
						}
                        if($model->save())
                                $this->redirect(array('view','id'=>$model->id));
						
						
                }
				else {
				return $this->render('create', [
					'model' => $model,
				
				]);
              
                
        }
		
		
		
	}
		/////////////////////////////////////////////////////
		
		
	
    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		if (!\Yii::$app->user->can('updateProject'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = $this->findModel($id);
		$emails = new Emails(); 
        
        if ($model->load(Yii::$app->request->post())){
						//$model->progress=implode(', ',$model->progress);//converting to string...
						$model->doneprogress=implode(', ',$model->doneprogress);//converting to string...
						$EmployeesThatInProject = implode(', ',\app\models\Employees::getEmployeesPerDays($model->employee));
						for($i=0; $i<sizeof($model->employee); $i++){
						////send email to employees
							$Mail = \app\models\Employees::getEmployeesEmailsToProjects($model->employee[$i]);
							$TeamLeaderName = \app\models\Employees::getTeamLeaderTprojects($model->team_leader);
							$value = Yii::$app->mailer->compose()
							->setFrom([ 'prat@webni.co.il' => 'prat@webni.co.il' ])
							->setTo($Mail)
							->setSubject(  'עדכון לפרויקט: '.$model->define_project)
							->setHtmlBody('<div style="text-align:right;"><h4>:שם הפרויקט</h4>'.$model->define_project.'<h4>:תיאור הפרויקט</h4>'.$model->description_project.'<h4>:ראש צוות</h4>'."$TeamLeaderName".'<h4>:עובדים ששובצו לפרויקט</h4>'.$EmployeesThatInProject.'<h4>:תאריך יעד</h4>'.$model->due_date.'<h4>:מיקום</h4>'.$model->location.'<h4>:הערות</h4>'.$model->notes.'</div>')
							->send();    
							$emails->save();
								
						}
							
							////send email to team leader
							$Mail = \app\models\Employees::getEmployeesEmailsToProjects($model->team_leader);
							$TeamLeaderName = \app\models\Employees::getTeamLeaderTprojects($model->team_leader);
							$value = Yii::$app->mailer->compose()
							->setFrom([ 'prat@webni.co.il' => 'prat@webni.co.il' ])
							->setTo($Mail)
							->setSubject(  'עדכון לפרויקט: '.$model->define_project)
							->setHtmlBody('<div style="text-align:right;"><h4>:שם הפרויקט</h4>'.$model->define_project.'<h4>:תיאור הפרויקט</h4>'.$model->description_project.'<h4>:ראש צוות</h4>'."$TeamLeaderName".'<h4>:עובדים ששובצו לפרויקט</h4>'.$EmployeesThatInProject.'<h4>:תאריך יעד</h4>'.$model->due_date.'<h4>:מיקום</h4>'.$model->location.'<h4>:הערות</h4>'.$model->notes.'</div>')
							->send();    
							$emails->save();
								
                            $model->employee = implode(", ", $model->employee);
                        
                if($model->save()){
                                return $this->redirect(['view', 'id' => $model->id]);
                        }
            
        } else {
                $model->employee = explode(', ',$model->employee);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('createUser')){
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
			}
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}