<?php

namespace app\controllers;

use Yii;
use app\models\SummaryDay;
use app\models\SummaryDaySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use miloschuman\highcharts\Highcharts;
use yii\filters\AccessControle;
use yii\web\UnauthorizedHttpException;
/**
 * SummaryDayController implements the CRUD actions for SummaryDay model.
 */
class SummaryDayController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				'only'=>['create','update','index','view','delete','export'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SummaryDay models.
     * @return mixed
     */
    public function actionIndex()
    {
	
		if (!\Yii::$app->user->can('indexSummary'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $searchModel = new SummaryDaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SummaryDay model.
     * @param string $id
     * @return mixed
     */
	 
	 public function actionExport()
    {
		$summaryExport= SummaryDay::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $summaryExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['date','israels','tourist','matmon','events','notes'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['date' => 'תאריך', 'israels' => 'כמות ישראלים','tourist' => 'כמות תיירים', 'matmon' => 'כמות מנויים', 'events' => 'אירועים ואכיפות','notes' =>'הערות'], 
			'fileName' => 'SummaryDay',
	
		]);
	}
	
	
	 
	 
	 
	 
	 
    public function actionView($id)
    {
		if (!\Yii::$app->user->can('viewSummary'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		if(Yii::$app->user->isGuest){
			return $this->goHome();
			}
			return $this->render('view', [
				'model' => $this->findModel($id),
			]);
    }

    /**
     * Creates a new SummaryDay model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if (!\Yii::$app->user->can('createSummary'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = new SummaryDay();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->date]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SummaryDay model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		if (!\Yii::$app->user->can('updateSummary'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->date]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SummaryDay model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('deleteSummary'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SummaryDay model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SummaryDay the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SummaryDay::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
