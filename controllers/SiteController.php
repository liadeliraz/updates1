<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ResetPassword;
use app\models\ContactForm;
use yii\web\UrlManager\Url;
use yii\db\Query;
use yii\db\Command;
use app\models\Emails;
use app\models\Employees;
use app\models\User;
use yii\web\Session;
use app\models\FormRecoverPass;
use app\models\FormResetPass;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
					'only' => ['login', 'logout'],
					'rules' => [
                    [
                        'actions' => ['login', 'logout'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
					
					  [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
					
					
					
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	
	private function randKey($str='', $long=0)
	{
		$key = null;
		$str = str_split($str);
		$start = 0;
		$limit = count($str)-1;
		for($x=0; $x<$long; $x++)
		{
			$key .= $str[rand($start, $limit)];
		}
		return $key;
	}
	
	
	///////////////////////////////////////////////////////////
	public function actionRecoverpass()
 {
  
  $model = new FormRecoverPass();
  
  
  $msg = null;
  
 if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
  {
    
   $table = User::find()->where("email =:email",[":email" => $model-> email]);
    
    //Si el usuario existe
    if ($table->count() == 1)
    {
     
     $session = new Session;
     $session->open();
     
     
     $session["recover"] = $this->randKey("abcdef0123456789", 200);
     $recover = $session["recover"];
     
     
    //$table = User::find()->where("email=:email", [":email" => $model->email])->one();
	//$table = User::find()->where(['email' =>  $model->email])->one();
	$table = User::find()->where("email =:email",[":email" => $model->email])->one();
	 
	 
     $session["id_recover"] = $table->id;    
     
     $verification_code = $this->randKey("abcdef0123456789",8);
     //Columna verification_code
     $table->verification_code = $verification_code;
     $table->save(false);
     
     //שליחה למייל
     $subject = "איפוס סיסמה";
     $body = "<div style='text-align:right;'><h3>,שלום וברכה</h3><p>:בהמשך לתהליך איפוס הסיסמה, קוד האימות הינו  ";
     $body .= "</div><strong>".$verification_code."</strong></p>";
     $body .= "<p><a href='http://prat.webni.co.il/app/web/site/resetpass'>לחץ לאיפוס הסיסמה</a></p>";

     //שליחת מייל
     Yii::$app->mailer->compose()
     ->setTo($model->email)
     ->setFrom(['prat@webni.co.il' => 'prat@webni.co.il' ])
     ->setSubject($subject)
     ->setHtmlBody($body)
     ->send();
     
     //Vaciar el campo del formulario
     $model->email = null;
     
     //Mostrar el mensaje al usuario
     Yii::$app->session->setFlash('successPmail','קוד האימות נשלח בהצלחה לכתובת המייל!');
	 return $this->redirect(['/site/index']);
    }
    else //El usuario no existe
    {
     $msg = "כתובת מייל שגויה!";
    }
   }
   else
   {
    $model->getErrors();
   }
  }
  return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
 }
 //////////////////////////////////////////////////////////////////////////////////
 public function actionResetpass()
 {
  
  $model = new FormResetPass;
  
  
  $msg = null;
  
  
  $session = new Session;
  $session->open();
  
  //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
  if (empty($session["recover"]) || empty($session["id_recover"]))
  {
   return $this->redirect(["site/index"]);
  }
  else
  {
   
   $recover = $session["recover"];
  
   $model->recover = $recover;
   
   $id_recover = $session["id_recover"];
   
  }
  
  
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {
    
    if ($recover == $model->recover)
    {
     
    $table = User::findOne(["email" => $model->email, "id" => $id_recover, "verification_code" => $model->verification_code]);
	//if(!empty($model->password))
    $table->password = $model->password;
	 //$table->password = crypt($model->password, Yii::$app->params['salt']);
	 
     if ($table->save(false))
     {
      
      
      $session->destroy();
      
      //Vaciar los campos del formulario
      $model->email = null;
      $model->password = null;
      $model->password_repeat = null;
      $model->recover = null;
      $model->verification_code = null;
      
     Yii::$app->session->setFlash('successNewpass','הסיסמא שונתה בהצלחה!');
	 return $this->redirect(['/site/login']);
      //$msg .= "<meta http-equiv='refresh' content='5; ".Url::toRoute("site/login")."'>";
     }
     else
     {
      $msg = '<div class="alert alert-success alert-dismissable">שגיאה!
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
     }
     
    }
    else
    {
     $model->getErrors();
    }
   }
  }
  
  return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
  
 }

	
	
	
	
/////////////////////////////////////////////////////////////////////////////
	
	

    public function actionIndex()
    {
        return $this->render('index');
    }

	
	 
	 
	 
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->homeUrl = ['site/index'];	
		Yii::$app->user->logout();

		return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}