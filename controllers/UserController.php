<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Employees;
use app\models\Emails;
use app\models\UserSearch;
use yii\grid\GridView;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use yii\web\Session;
use yii\filters\AccessControle;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
       return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete','export','change_password'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		//if (!\Yii::$app->user->can('indexUser') && 
		  //  !\Yii::$app->user->can('indexOwnUser') )
			//throw new UnauthorizedHttpException ('Hey, You are not allowed to update users'); 
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
        ]);
    }

	public function actionExport()
    {
		$userExport= User::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $userExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['id','username',['attribute' => 'role','value' => function($model) {
                        return $model->userole;
                    }]], //without header working, because the header will be get label from attribute label. 
			'headers' => ['id' => 'ת.ז', 'username' => 'שם משתמש','role' =>'תפקיד'], 
			'fileName' => 'Users',
	
		]);
	}
	
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$model = $this->findModel($id);
		if (!\Yii::$app->user->can('viewUser') && 
		    !\Yii::$app->user->can('viewOwnUser', ['user' =>$model]) )
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	 
	 public function actionCreate()
    {
       //access control
		if (!\Yii::$app->user->can('createUser'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		$model = new User();
		if ($model->load(Yii::$app->request->post())) {
		$emailEmployee = \app\models\Employees::find()->where(['id' => $model->id])->one();
		//$eail = \app\models\Employees::getEmailemployeeTuser($model->id);
		$model->email = $emailEmployee->email;
		}
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
		
		$Mail = \app\models\Employees::getEmailemployeeTuser($model->id);
			$employeeName = \app\models\Employees::getTeamLeaderTprojects($model->id);
			$value = Yii::$app->mailer->compose()
			->setFrom([ 'prat@webni.co.il' => 'prat@webni.co.il' ])
			->setTo($Mail)
			->setSubject('משתמש חדש')
			->setHtmlBody('<div style="text-align:right;"><h3>,שלום וברכה'." ".$employeeName.'</h3><p>.תהליך הפקת הסיסמה הסתיים בהצלחה</p><h4>:שם משתמש</h4>'.$model->username.'<h4>:סיסמה</h4>'.$model->password.'</div>')
			->send();    
			
			//$eail = \app\models\Employees::findOne($model->id);
			//$model->email = "test";
			//$model->email = \app\models\Employees::getEmailemployeeTuser($model->id);
			$model->save();
            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles();
            return $this->render('create', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    }
	 /*
    public function actionCreate()
    {
       //access control
		if (!\Yii::$app->user->can('createUser'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
		$model = new User();		
       
        if ($model->load(Yii::$app->request->post())) 
		{
			$Mail = \app\models\Employees::getEmailemployeeTuser($model->id);
			$employeeName = \app\models\Employees::getTeamLeaderTprojects($model->id);
			$value = Yii::$app->mailer->compose()
			->setFrom([ 'prat@webni.co.il' => 'prat@webni.co.il' ])
			->setTo($Mail)
			->setSubject('משתמש חדש')
			->setHtmlBody('<div style="text-align:right;"><h3>,שלום וברכה'." ".$employeeName.'</h3><p>.תהליך הפקת הסיסמה הסתיים בהצלחה</p><h4>:שם משתמש</h4>'.$model->username.'<h4>:סיסמה</h4>'.$model->password.'</div>')
			->send();    
			
			//$eail = \app\models\Employees::find()->where(['id' => $model->id])->one();
			//$model->email = $eail->email;
			//$model->email = \app\models\Employees::getEmailemployeeTuser($model->id);
			$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles();
            return $this->render('create', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    }*/

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
	 
	 public function actionChange_password()
	 {
		 
		//use up user and load post dataProvider
		 $user = Yii::$app->user->identity;
		  $loadedPost = $user->load(Yii::$app->request->post());
		 
		 if($loadedPost && $user->validate()){
			 
			 $user->password = $user->newPassword;
			 //save, set flash and refresh page
			 $user->save(false);
			 #var_dump($user->errors);
			Yii::$app->session->setFlash('success',' הסיסמא שונתה בהצלחה!');
				//\Yii::app()->user->setFlash('success', "Data1 saved!");
			return $this->redirect(['/site/index']);
			 
		 }
		
		 return $this->render("change_password",[
		      'user' => $user,
	     ]);
	 
	 }
	 
	  
	 
	 
    public function actionUpdate($id)
    {
		
        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post())&& $model->save(false)) {		
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles();
            return $this->render('update', [
                'model' => $model,
				'roles' => $roles,
            ]);
        }
    
	}

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (!\Yii::$app->user->can('deleteUser'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
  /*  protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }*/
 protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
				$roles = \Yii::$app->authManager->getRolesByUser($model->id);
				$model->role = array_keys($roles)[0];
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	}