<?php

namespace app\controllers;

use Yii;
use app\models\Events;
use app\models\Employees;
use app\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\bootstrap\Modal;
use yii\widget\Pjax;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;
use app\models\Emails;
use yii\filters\AccessControle;




/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller

{
    public function behaviors()
    {
        return [
		'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
	 
	
public function actionIndexapi()
    {
	//return $this->redirect(['indexapi']);
	return $this->render('indexapi');
	}

    public function actionIndex()
    {
		
        $events = Events::find()->all();
		$tasks = [];
		
		
		
        foreach ($events as $eve) 
        {
			$tmoora = false;
			$mahala = false;
			$hofesh = false;
			$managerView = true;
			
		  if (strpos($eve->employees, 'T') !== false) {
		  $tmoora = true;
		  	if (!\Yii::$app->user->can('createUser'))
			$managerView = false;
		  }
		  if (strpos($eve->employees, 'M') !== false) {
		  $mahala = true;
		  if (!\Yii::$app->user->can('createUser'))
			$managerView = false;
		  }
		  if (strpos($eve->employees, 'H') !== false) {
		  $hofesh = true;
		  if (!\Yii::$app->user->can('createUser'))
			$managerView = false;
		  }
		  if($managerView){	
          $event = new \yii2fullcalendar\models\Event();
		  $color = \app\models\Employees::findOne($eve->employees)->color;
          $event->backgroundColor = $color;
		  $event->className = 'btn';
		  $event->id = "$eve->created_date,$eve->employees";
		  //$eve->employees=explode(', ',$eve->employees);//converting to array...
		  $counter = count($eve->employees);
         // $event->title = "$eve->employees  $eve->created_date";
		   //$event->title = $eve->employees[0];
			
		  $name = \app\models\Employees::findOne($eve->employees)->fullname;
		  $role = \app\models\Employees::findOne($eve->employees)->roleItem->name;
		  $event->title = "$name";
		  if($tmoora)
		  $event->title = " $name  - יום תמורה";
		  if($mahala)
		  $event->title = " $name  - יום מחלה";
		  if($hofesh)
		  $event->title = " $name  - יום חופש";
		  
		  if("$name" == "$eve->team_leader"){
		  $event->title = "$name - ראש צוות";
		  }
		  $resultCashier = explode(',',$eve->cashier );
		  for ($c = 0; $c <sizeof($resultCashier) ; $c++){
		  if("$eve->employees" == "$resultCashier[$c]")
		   $event->title = "$name - קופאי";
		  }
		  $event->start = $eve->created_date;
          $tasks[] = $event;
		  }
		  }
		
		  
		   return $this->render('index', [
		
            'events' => $tasks,
        ]);
    }
	

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	
    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($date,$monthName)
    {	
	
	if(isset($_POST['Events']))
		 {
						$models = \app\models\Events::find()->where(['created_date' => "$date"])->all();
						foreach ($models as $model2) {
						$model2->delete();

						}
						}
		
		$getCashierArr = [];
		$getCashierId = [];
		$getTTid = "";
		$result = 1;
		for ($i = 0; $i <= $result; $i++) {
		$model = new Events();
		$model->created_date = $date;
		$model->month = $monthName;
		
		if(isset($_POST['Events']))
		 {
						
                        $model->attributes=$_POST['Events'];
						$result = count($model->employees);
						$result = $result - 1;
                        if($model->employees!=='')
                                $model->employees=$model->employees[$i];//implode(', ',$model->employees);//converting to string...
								for($c=0; $c<sizeof($model->cashier);$c++){
								$getCashierArr[$c] = $model->cashier[$c];
								$getCashierId[$c] = \app\models\Employees::getEmployeesID($model->cashier[$c]);
								}
								/*// if($model->team_leader!==''){
								$arrfullname = explode(' ',$_POST['team_leader']);
								$IDEmployees = \app\models\Employees::find()->where(['first_name' => "$arrfullname[0]",'last_name'=>"$arrfullname[1]"])->one();
								$model->team_leader = $IDEmployees->id;
								//}*/
								$model->cashier=implode(', ',$getCashierId);
								$model->id = "$date,$model->employees";
                        if($model->save())
							$this->redirect(['index']);
                }
        
         else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
	}
	
	
    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
		
    }
	
	 public function actionFullscreen()
    {
$events = Events::find()->all();
		$tasks = [];

        foreach ($events as $eve) 
        {
		 
          $event = new \yii2fullcalendar\models\Event();
		  $color = \app\models\Employees::findOne($eve->employees)->color;
         // $event->BorderColor = $color;
		  $event->className = 'btn';
		  $event->id = "$eve->created_date,$eve->employees";
		
		  $counter = count($eve->employees);
		  $name = \app\models\Employees::findOne($eve->employees)->fullname;
		  $role = \app\models\Employees::findOne($eve->employees)->roleItem->name;
		  $event->title = "$name";
		  if("$eve->employees" == "$eve->team_leader"){
		  $event->title = "$name - ראש צוות";
		  }
		  $resultCashier = explode(',',$eve->cashier );
		  for ($c = 0; $c <sizeof($resultCashier) ; $c++){
		  if("$eve->employees" == "$resultCashier[$c]")
		   $event->title = "$name - קופאי";
		  }
		  $event->start = $eve->created_date;
          $tasks[] = $event;
		  }
		  
		   return $this->render('fullscreen', [
		
            'events' => $tasks,
        ]);
    }
		
    

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionReport() {
    // get your HTML raw content without any layouts or scripts
    //$content = $this->renderPartial('index');
	 $events = Events::find()->all();
    $content = \yii2fullcalendar\yii2fullcalendar::widget(array(
		 'clientOptions' => [
		 //'eventClick' => new JsExpression($JSEventClick),
		  ],
      'events'=> $events,
	        'options' => [
        'lang' => 'he',
		'dayNames' => true,
		
		
					],
  ));


    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => ['title' => 'Krajee Report Title'],
         // call mPDF methods on the fly
        'methods' => [ 
            'SetHeader'=>['Krajee Report Header'], 
            'SetFooter'=>['{PAGENO}'],
        ]
    ]);
    
    // return the pdf output as per the destination setting
    return $pdf->render(); 
}

public function actionUpload() //העלאת קבצים (לא תמונה)
	{	
		
	
		
		return $this->render('index');
		}
		
		public function actionUpload2() 
	{	
		return $this->render('index');
		}
		
		public function actionAllview() //העלאת קבצים (לא תמונה)
		{	
		return $this->render('allview');
		}

		public function actionDetails($month)
    {
       return $this->render('update', [
                'month' => $month,
            ]);
        
    }
		
        
 
    
	public function actionLists($id)
    {
        $emlist = explode(',',$id);
		
        if($emlist>0){
             for($el = 0; $el<sizeof($emlist);$el++ ){
			 if (strpos($emlist[$el], 'T') !== false) {
		  if(($key100 = array_search($emlist[$el], $emlist)) !== false) {
				unset($emlist[$key100]);
					}
		  }
		  if (strpos($emlist[$el], 'H') !== false) {
		  if(($key100 = array_search($emlist[$el], $emlist)) !== false) {
				unset($emlist[$key100]);
					}
		  }
		  if (strpos($emlist[$el], 'M') !== false) {
		  if(($key100 = array_search($emlist[$el], $emlist)) !== false) {
				unset($emlist[$key100]);
					}
		  }
			 $name = \app\models\Employees::findOne($emlist[$el])->fullname;
                echo "<option value='$name'>$name</option>";
            }
        }
        else{
            echo "<option>-</option>";
        }
 
    }
	
	
	
			
		
		public function actionExport()
    {
		/*$dt = \new DateTime("מאי 2017");
		$year = $dt->format('Y');
		$month = $dt->format('m');
		$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month,$year); // 31*/
		//$newDate = date("d", strtotime('1/05/2017'));
		
		/*$createdArray = [];
		
		$dt = \new DateTime("1/05/2017");
		$year = $dt->format('Y');
		$month = $dt->format('m');
		$day = $dt->format('d');

		$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month,$year); // 31*/
		
		/*for($ww=0;$ww<=$daysInMonth;$ww++){
			
		}
		
		$getEArr = \app\models\Events::find()->where(['month'=>'מאי 2017'])->all();
		
		$a100Array = ArrayHelper::getColumn($getEArr, 'id');
		
$QAquery = \app\models\Events::find()->select('created_date')->where(['created_date'=>$emlist[$i]])->all();
		
		
		foreach($getEArr as $eee){
		$createdArray[] = $eee->created_date ;
		
		
		}
		$result = array_unique($createdArray);
		for($q=0;$q<sizeof($result);$q++){
		$QAquery = \app\models\Events::find()->select('employees')->where(['created_date'=>$result[$q]])->all();
		
			}
		if (strpos($string, ',') !== false){
		$kopaim = explode(', ',$result); //convert string to array
		$kopaim = implode(', ',$result); //	//convert array to string 	
		}*/
		//$dt = new DateTime("1/05/2017");	
		$eventsExport= Events::find()->all();
		$getEArr = \app\models\Events::find()->where(['month'=>'מאי 2017'])->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $getEArr,
			'format' => 'Excel2007',
			'mode' => 'export', //default value as 'export'
			'columns' => ['created_date',/*['attribute' => 'created_date','value' => function($getEArr) {
						$dayinmonth = ArrayHelper::getColumn($getEArr, 'created_date');
                        for($ww=0;$ww<=$dayinmonth;$ww++){
						echo date("d", strtotime('dayinmonth[$ww]'));
			
		}
                    }]*//*['attribute' => 'created_date','value' => function($getEArr) {
						$dayinmonth = ArrayHelper::getColumn($getEArr, 'created_date');
                        for($ww=0;$ww<=$dayinmonth;$ww++){
						echo date("w", strtotime('dayinmonth[$ww]'));
			
		}
                    }]*/'team_leader',], //without header working, because the header will be get label from attribute label. 
			'headers' => ['created_date' => 'תאריך' ], 
			'fileName' => 'שיבוץ',
	
		]);
	}
		
		
		
	}
    


