<?php

namespace app\controllers;

use Yii;
use app\models\Employees;
use app\models\EmployeesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Role;
use app\models\Armed;
use app\models\ImageManager;
use app\models\PercentOfJobs;
use yii\web\UnauthorizedHttpException;
use yii\filters\AccessControle;
use yii\web\UploadedFile;
use app\models\UploadForm;
use dosamigos\fileupload\FileUpload;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\Session;

/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
         return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete','export','files'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	
	
    /**
     * Lists all Employees models.
     * @return mixed
     */
     public function actionIndex()
    {
        
		if (!\Yii::$app->user->can('indexEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
		$searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'roles' => Role::getRolesWithAllRoles(),
			'role' => $searchModel->role,
			'armeds' => Armed::getArmedsWithAllArmeds(),
			'armed' => $searchModel->armed,
			'PercentOfJobss' => PercentOfJobs::getPercentOfJobssWithAllPercentOfJobss(),
			'Percent_of_jobs' => $searchModel->Percent_of_jobs,
			'statuss' => Employees::getstatus(),
			'status' => $searchModel->status,
		
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     */
	 
	 
	 public function actionExport()
    {
		$employeeExport= Employees::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $employeeExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['id','first_name','last_name','cellphone','adress',['attribute' => 'role','value' => function($model) {
                        return $model->roleItem->name;
                    }],['attribute' => 'armed','value' => function($model) {
                        return $model->armedItem->name;
                    }],['attribute' => 'Percent_of_jobs','value' => function($model) {
                        return $model->percent_of_jobsItem->name;
                    }],'email',['attribute' => 'status','value' => function($model){
				if($model->status == 0)
				$model->status = "פעיל";
				if($model->status == 1)
				$model->status = "לא פעיל";
				
					return $model->status ;
				}]], //without header working, because the header will be get label from attribute label. 
			'headers' => ['id' => 'ת.ז', 'first_name' => 'שם פרטי','last_name' => 'שם משפחה', 'cellphone' => 'פלאפון', 'adress' => 'כתובת', 'role' => 'תפקיד', 'armed' => 'סוג נשק','Percent_of_jobs' =>'אחוז משרה','email'=>'מייל','status'=>'סטטוס'], 
			'fileName' => 'Employees',
	
		]);
	}
	 
    public function actionView($id)
    {
		if (!\Yii::$app->user->can('viewEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionFiles($id)
    {
		
        return $this->render('files', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       

		if (!\Yii::$app->user->can('createEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		$model = new Employees();

        if ($model->load(Yii::$app->request->post())) 
		{
			     
			     
					 $employeeId = $model->id;
					 $image= UploadedFile::getInstance($model, 'image'); //מקבל מופע של העלאת התמונה
					 if(!empty($image))
						{
							$imgName = 'employee_' . $employeeId . '.' . $image->getExtension();
							$image->saveAs('Employees_images/' .$imgName);
							$model->image = $imgName;
							
						 }
						
				$model->save();
	            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Employees model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	
	
        $model = $this->findModel($id);

		if (!\Yii::$app->user->can('updateEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		$current_image = $model->image;
        if ($model->load(Yii::$app->request->post()) ) 
		{
			if($model->status == "1"){
			$userAccess = \app\models\User::findOne($model->id);
			$userAccess->delete();
			Yii::$app->session->setFlash('success22','הגדרת את'."  -".$model->fullname . " כעובד לא פעיל!  גישתו למערכת נחסמה, לשם מתן גישה חדשה יש לעדכן לסטטוס פעיל וליצור משתמש חדש בתפריט משתמשים.");
			}
			 $employeeId = $model->id;
			 $image= UploadedFile::getInstance($model, 'image');
			 if(!empty($image) && $image->size !== 0) {
            //print_R($image);die;
            $imgName = 'employee_' . $employeeId . '.' . $image->getExtension();
			$image->saveAs('Employees_images/' .$imgName);
            $model->image = $imgName;
			
        }
        else
            $model->image = $current_image;
        $model->save();
		return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Employees model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	
		if (!\Yii::$app->user->can('deleteEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
	
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	public function actionUploadFiles() //העלאת קבצים (לא תמונה)
	{	
		//$TestModel = \app\models\ImageManager::find()->all();
		if(Yii::$app->request->isPost){
			$post = Yii::$app->request->post();
			$dir =  'Employees_files/';
			if(!file_exists($dir)){
				FileHelper::createDirectory($dir);
			}
			$result_link = 'Employees_files/';
			$model= new ImageManager();
			$file = UploadedFile:: getInstanceByName('ImageManager[attachment]');
			//$file= UploadedFile::getInstance($model, 'ImageManager[attachment]'); //מקבל מופע של העלאת התמונה
			
			//$model->name = 'attachment_number'.'_'.Yii::$app->getSecurity()->generateRandomString(6);
			//$name = EmployeesController::encodeToUtf8($file->name);
			$name = iconv('UTF-8', 'HEBREW//TRANSLIT', ($file->name));
			$model->name = $file->name;
			//$model->name = $file->name;
				
			$model->save();
			$model->load($post);
			$model->validate();
			$dir =  'Employees_files/'.$model->item_id .'/';
			if (!is_dir($dir)) {
				mkdir($dir);         
					}
			if($model->hasErrors()){
				$result =[
					'error' => $model->getFirstError('file')
				];
			}else{
				if($file->saveAs($dir . $name)){
				  //$imag = Yii::$app->file->load($dir . $name);
				  //$imag->resize(800,NULL,Yii\image\drivers\Image::PRECISE)->save($dir . $name, 85);
				   $result = ['filelink' => $result_link . $file->name, 'filename'=>$file->name];
				}else{
					$result =[
					   'error' => 'טעות'
					];
					
				}
				$model->save();
			}
			Yii::$app->response->format = Response::FORMAT_JSON;
			return $result;	
		}else{
			//throw new BadRequestHttpException('רק פוסט מותר ');
			return "הקובץ עלה בהצלחה";
		}
	
	
	}
	
	
	
	
	
	
	
	
    /**
     * Finds the Employees model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employees the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employees::findOne($id)) !== null) {
           
			return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
	
	
	
	
	
	
}